package info.textgrid.lab.woerterbuchsearchmask.view;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.woerterbuchsearchmask.view.messages"; //$NON-NLS-1$
	public static String DictionarySearchMaskView_Exact;
	public static String DictionarySearchMaskView_Fuzzy;
	public static String DictionarySearchMaskView_Keyword;
	public static String DictionarySearchMaskView_Limit;
	public static String DictionarySearchMaskView_Linked_Lemmas_Only;
	public static String DictionarySearchMaskView_Max_Limit;
	public static String DictionarySearchMaskView_Max_Limit_Value;
	public static String DictionarySearchMaskView_Search_Parameters;
	public static String DictionarySearchMaskView_Search_Processing;
	public static String DictionarySearchMaskView_Searchtype;
	public static String DictionarySearchMaskView_Select_Dict;
	public static String DictionarySearchMaskView_Service_Not_Available;
	public static String DictionarySearchMaskView_Specify_Keyword;
	public static String DictionarySearchMaskView_Start_Search;
	public static String DictionarySearchMaskView_WB_List;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
