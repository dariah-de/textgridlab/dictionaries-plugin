package info.textgrid.lab.woerterbuchsearchmask.view;

import info.textgrid.lab.dictionarysearch.Activator;
import info.textgrid.lab.dictionarysearch.DSLemma;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebServiceSoap_wbb_getAllDictionarys_RequestStruct;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebServiceSoap_wbb_getAllDictionarys_ResponseStruct;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebService_Impl;
import info.textgrid.lab.dictionarysearch.views.DictionarySearchView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

//import org.apache.axis2.transport.http.HTTPConstants;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Dictionary Search Mask View. Holds a gui to specify a search in the
 * "W�rterbuchnetz" in Trier. Possible parameters are: searchlemma, limit of
 * results, searchtype. desired dictionary to search in can be selected from a
 * tree viewer.
 * 
 * @author wick
 * 
 */
public class DictionarySearchMaskView extends ViewPart {

	private static final String TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER = "http://tg-woerterbuch-webservice.uni-trier.de/Wbb_WebService/"; //$NON-NLS-1$
//	private static final String TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER = "http://urts173.uni-trier.de:8115/Wbb_WebService/";
	private static final int READ_TIME_OUT = 8*1000;
	
	private static final String SEARCH_PROCESSING = Messages.DictionarySearchMaskView_Search_Processing;


	public static String ID = "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView"; //$NON-NLS-1$

	/**
	 * constructor
	 */
	public DictionarySearchMaskView() {

	}

	private CheckboxTreeViewer treeViewer = null;
	private Label labelSearchstring = null;
	private Text lemma = null;
	private Label labelLimit = null;
	private Text textLimit = null;
	private Label labelSearchForm = null;
	private Button buttonSearchFormExact = null;
	private Button buttonSearchFormFuzzy = null;
	private Button buttonSearchStart = null;
	private static String LIMIT = "10"; //$NON-NLS-1$
	private static String LINKEDLEMMAS = "false"; //$NON-NLS-1$
	private String suchArt = null;
	private Button buttonLinkedLemma;
	private Label labelLinkedLemmaText;
	private Label labelSearchStatus = null;
	private Composite top;

	private DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();
	//not needed for version 1.0
	private boolean withWBLE = false;

	@Override
	public void createPartControl(final Composite parent) {

		top = new Composite(parent, SWT.BORDER);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		gridLayout.makeColumnsEqualWidth = false;
		top.setLayout(gridLayout);
		
		Group groupSearchParameters = new Group(top, SWT.NONE);
		groupSearchParameters.setText(Messages.DictionarySearchMaskView_Search_Parameters);
		GridLayout searchLayout4SearchParameters = new GridLayout();
		groupSearchParameters.setLayout(searchLayout4SearchParameters);
		searchLayout4SearchParameters.numColumns = 3;
		searchLayout4SearchParameters.makeColumnsEqualWidth = false;
		
		buttonSearchStart = new Button(top, SWT.PUSH);
		buttonSearchStart.setText(Messages.DictionarySearchMaskView_Start_Search);
		buttonSearchStart.setFocus();
		// Group searchFactsGroupButton = new Group(parent, SWT.NONE);
		
		Group groupSearchFacts = new Group(top, SWT.NONE);
		groupSearchFacts.setText(Messages.DictionarySearchMaskView_Limit);
		GridLayout searchLayout4SearchFacts= new GridLayout();
		groupSearchFacts.setLayout(searchLayout4SearchFacts);
		searchLayout4SearchFacts.numColumns = 1;
		searchLayout4SearchFacts.makeColumnsEqualWidth = false;
		// searchFactsGroupButton.setLayout(new GridLayout());

//		Group groupSearchParameters2 = new Group(parent, SWT.NONE);
//		GridLayout searchLayout2 = new GridLayout();
//		groupSearchParameters.setLayout(searchLayout2);
		// GridData gd0 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		// searchFactsGroup1.setLayoutData(gd0);

		
		Composite compositeSearchParameters = new Composite(groupSearchParameters, SWT.NULL);
		compositeSearchParameters.setLayout(new GridLayout(1,false));

		Composite compositeKeyword = new Composite(compositeSearchParameters, SWT.NULL);
		compositeKeyword.setLayout(new GridLayout(2,false));
		Composite compositeSearchType = new Composite(compositeSearchParameters, SWT.NULL);
		compositeSearchType.setLayout(new GridLayout(2,false));
		labelSearchstring = new Label(compositeKeyword, SWT.NONE);
		labelSearchstring.setText(Messages.DictionarySearchMaskView_Keyword);
		lemma = new Text(compositeKeyword, SWT.BORDER);
		lemma.setLayoutData(new GridData(120, SWT.DEFAULT));
		lemma.setSize(200, 20);
		//Label fillLabel = new Label(searchFactsGroup1, SWT.NONE);
		

		
		Composite compositeLimit = new Composite(groupSearchFacts, SWT.NULL);
		compositeLimit.setLayout(new GridLayout(1, false));
		
		Composite compositeLimitCount = new Composite(compositeLimit, SWT.NULL);
		compositeLimitCount.setLayout(new GridLayout(3,false));

		labelLimit = new Label(compositeLimitCount, SWT.NONE);
		labelLimit.setText(Messages.DictionarySearchMaskView_Limit);
		textLimit = new Text(compositeLimitCount, SWT.BORDER);
		textLimit.setText(LIMIT);
		textLimit.setLayoutData(new GridData(25, SWT.DEFAULT));
		textLimit.setToolTipText(Messages.DictionarySearchMaskView_Max_Limit);
		textLimit.setTextLimit(4);
		textLimit.pack();
		textLimit.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				Object src = e.getSource();

				if (src == textLimit) {
					if (e.text.matches("^\\D+$")) { //$NON-NLS-1$
						e.doit = false;
						showMessage("Numbers only! Attempted input: " + e.text); //$NON-NLS-1$
					}
				}
			}
		});
//		Label fillLabel1 = new Label(composite2, SWT.NONE);
//		fillLabel1.setSize(1,1);
		labelSearchForm = new Label(compositeSearchType, SWT.NONE);
		labelSearchForm.setText(Messages.DictionarySearchMaskView_Searchtype);

		Composite compositeSearchTypeButtons = new Composite(compositeSearchType, SWT.NULL);
		compositeSearchTypeButtons.setLayout(new RowLayout());
		buttonSearchFormExact = new Button(compositeSearchTypeButtons, SWT.RADIO);
		buttonSearchFormExact.setText(Messages.DictionarySearchMaskView_Exact);
		buttonSearchFormExact.setSelection(true);
		buttonSearchFormFuzzy = new Button(compositeSearchTypeButtons, SWT.RADIO);
		buttonSearchFormFuzzy.setText(Messages.DictionarySearchMaskView_Fuzzy);
		
		Composite compositeLinkedLemmasOnlyOutline = new Composite(compositeLimit, SWT.NULL);
		compositeLinkedLemmasOnlyOutline.setLayout(new GridLayout(1, false));
		
		Composite compositeLinkedLemmasOnly = new Composite(compositeLinkedLemmasOnlyOutline, SWT.NULL);
		compositeLinkedLemmasOnly.setLayout(new GridLayout(1, false));

		if (withWBLE ) {
			//not needed in version 1.0			

			buttonLinkedLemma = new Button(compositeLinkedLemmasOnly, SWT.CHECK);
			buttonLinkedLemma.setText(Messages.DictionarySearchMaskView_Linked_Lemmas_Only);
			//Label fillLabel = new Label(top, SWT.NONE);
		} else {
			Label labelWBLEfalse = new Label(compositeLinkedLemmasOnly, SWT.NONE);
			labelWBLEfalse.setText(" "); //$NON-NLS-1$
		}
		
		labelSearchStatus = new Label(compositeLimitCount, SWT.NONE);
		GridData gridData = new GridData(105, SWT.DEFAULT);
		labelSearchStatus.setLayoutData(gridData);
		labelSearchStatus.setText(" "); //$NON-NLS-1$
		

		
		treeViewer = new CheckboxTreeViewer(top);
		GridData treeGridData = new GridData();
		treeGridData.horizontalAlignment = SWT.FILL;
		treeGridData.horizontalSpan = 3;
		treeGridData.verticalAlignment = SWT.FILL;
		treeGridData.grabExcessVerticalSpace = true;
		treeGridData.grabExcessHorizontalSpace = true;

		treeViewer.getTree().setLayoutData(treeGridData);
		treeViewer.setContentProvider(new ITreeContentProvider() {
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof List) {
					return ((List) parentElement).toArray();
				} else if (parentElement instanceof Library) {
					return ((Library) parentElement).getDictionaryGroups()
							.toArray();
				} else if (parentElement instanceof DictionaryGroup) {
					return ((DictionaryGroup) parentElement).getDictionaries()
							.toArray();
				}
				return new Object[0];
			}

			public Object getParent(Object element) {
				if (element instanceof DictionaryGroup) {
					return ((DictionaryGroup) element).getAssociatedLibrary();
				} else if (element instanceof DictionaryW) {
					return ((DictionaryW) element)
							.getAssociatedDictionaryGroup();
				} else
					return null;
			}

			public boolean hasChildren(Object element) {
				if (element instanceof List) {
					return ((List) element).size() > 0;
				} else if (element instanceof Library) {
					return ((Library) element).getDictionaryGroups().size() > 0;
				} else if (element instanceof DictionaryGroup) {
					return ((DictionaryGroup) element).getDictionaries().size() > 0;
				} else
					return false;
			}

			public Object[] getElements(Object inputElement) {
				return getChildren(inputElement);
			}

			public void dispose() {
			}

			public void inputChanged(Viewer viewer, Object oldInput,
					Object newInput) {
			}
		});
		treeViewer.setLabelProvider(new TestLabelProvider());
		treeViewer.addCheckStateListener(new ICheckStateListener() {
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (event.getChecked()) {
					treeViewer.setSubtreeChecked(event.getElement(), true);
					getCheckedDictionaries();
				} else {
					treeViewer.setSubtreeChecked(event.getElement(), false);
					getCheckedDictionaries();
				}
			}
		});
		try {
			treeViewer.setInput(initModel());
		} catch (SAXException e2) {
			// TODO Auto-generated catch block
			IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"SAXException in DictionarySearchMaskView (treeViewer.setInput(initModel());", //$NON-NLS-1$
					e2);
			Activator.getDefault().getLog().log(status);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"IOException in DictionarySearchMaskView (treeViewer.setInput(initModel());", //$NON-NLS-1$
					e2);
			Activator.getDefault().getLog().log(status);
		} catch (ParserConfigurationException e2) {
			// TODO Auto-generated catch block
			IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"ParseConfigurationException in DictionarySearchMaskView (treeViewer.setInput(initModel());", //$NON-NLS-1$
					e2);
			Activator.getDefault().getLog().log(status);
		}
		treeViewer.expandAll();
		treeViewer.setAllChecked(true);
		treeViewer.getControl().setBackground(new Color(null, 238, 238, 238));

		lemma.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR && checkAllNecessary()) {
					if (buttonSearchFormExact.getSelection())
						suchArt = "e"; //$NON-NLS-1$
					else
						suchArt = "u"; //$NON-NLS-1$
					if (textLimit.getText() != "") //$NON-NLS-1$
						LIMIT = textLimit.getText();
					labelSearchStatus.setForeground(new Color (Display.getCurrent (), 0, 0, 255));
					labelSearchStatus.setText(SEARCH_PROCESSING);
					Cursor waitCursor = new Cursor(Display.getCurrent(),SWT.CURSOR_WAIT);
					parent.setCursor(waitCursor);
					
					if (withWBLE) {
						//not needed in version 1.0
						if (buttonLinkedLemma.getSelection()) {
							LINKEDLEMMAS = "true"; //$NON-NLS-1$
							textLimit.setText("1000"); //$NON-NLS-1$
						} else {
							LINKEDLEMMAS = "false"; //$NON-NLS-1$
							textLimit.setText(LIMIT);
						}
					} 
					DictionarySearchView dsv = (DictionarySearchView) getSite()
							.getPage().findView(DictionarySearchView.ID);
					// showMessage("dsv = null: die Erste");

					if (dsv == null) {
						dsv = (DictionarySearchView) PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage()
								.findView(DictionarySearchView.ID);
						// showMessage("dsv = null: die Zweite");
					}
					if (dsv == null)
						try {
							dsv = (DictionarySearchView) getSite().getPage()
									.showView(DictionarySearchView.ID);
							// does not return null, so it's either success
							// or
							// an exception.
						} catch (PartInitException exp) {
							showMessage("Exception!!!!!"); //$NON-NLS-1$
						}
					if (dsv == null)
						showMessage("dsv immer noch = null :-("); //$NON-NLS-1$

					DSLemma newLemma = new DSLemma(lemma.getText());
					dsv.setOffset("0"); //$NON-NLS-1$
					dsv.setLimit(textLimit.getText());
					dsv.setSearchType(suchArt);
					//dsv.setLinkedLemmas(LINKEDLEMMAS);
					dsv.setWbbList(getCheckedDictionariesString());
					dsv.setPageNo(1);
					dsv.setSearchFromOutside(true);
					dsv.getTreeViewer().setInput(newLemma);
					dsv.getTreeViewer().expandAll();
					dsv.setNewLabelText(lemma.getText());
					labelSearchStatus.setText(" "); //$NON-NLS-1$
					Cursor normalCursor = new Cursor(Display.getCurrent(),SWT.CURSOR_ARROW);
					parent.setCursor(normalCursor);
				}
			}

			public void keyReleased(KeyEvent e) {
			}

		});
		buttonSearchStart
				.addMouseListener(new org.eclipse.swt.events.MouseAdapter() {
					@Override
					public void mouseDown(org.eclipse.swt.events.MouseEvent e) {
						if (checkAllNecessary()) {
							if (buttonSearchFormExact.getSelection())
								suchArt = "e"; //$NON-NLS-1$
							else
								suchArt = "u"; //$NON-NLS-1$
							if (textLimit.getText() != "") //$NON-NLS-1$
								LIMIT = textLimit.getText();
							if (withWBLE) {
								//not needed in version 1.0							
								if (buttonLinkedLemma.getSelection()) {
									LINKEDLEMMAS = "true"; //$NON-NLS-1$
									textLimit.setText("1000"); //$NON-NLS-1$
								} else {
									LINKEDLEMMAS = "false"; //$NON-NLS-1$
									textLimit.setText(LIMIT);
								}
							} 
								
							labelSearchStatus.setForeground(new Color (Display.getCurrent (), 0, 0, 255));
							labelSearchStatus.setText(SEARCH_PROCESSING);
							Cursor waitCursor = new Cursor(Display.getCurrent(),SWT.CURSOR_WAIT);
							parent.setCursor(waitCursor);
							DictionarySearchView dsv = (DictionarySearchView) getSite()
									.getPage()
									.findView(DictionarySearchView.ID);
							// showMessage("dsv = null: die Erste");

							if (dsv == null) {
								dsv = (DictionarySearchView) PlatformUI
										.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage().findView(
												DictionarySearchView.ID);
								// showMessage("dsv = null: die Zweite");
							}
							if (dsv == null)
								try {
									dsv = (DictionarySearchView) getSite()
											.getPage().showView(
													DictionarySearchView.ID);
									// does not return null, so it's either
									// success
									// or
									// an exception.
								} catch (PartInitException exp) {
									showMessage("Exception!!!!!"); //$NON-NLS-1$
								}
							if (dsv == null)
								showMessage("dsv immer noch = null :-("); //$NON-NLS-1$

							DSLemma newLemma = new DSLemma(lemma.getText());
							dsv.setOffset("0"); //$NON-NLS-1$
							dsv.setLimit(textLimit.getText());
							dsv.setSearchType(suchArt);
							//dsv.setLinkedLemmas(LINKEDLEMMAS);
							dsv.setWbbList(getCheckedDictionariesString());
							dsv.setPageNo(1);
							dsv.setSearchFromOutside(true);
							labelSearchStatus.setText(SEARCH_PROCESSING);
							dsv.getTreeViewer().setInput(newLemma);
							dsv.getTreeViewer().expandAll();
							dsv.setNewLabelText(lemma.getText());
							labelSearchStatus.setText(" "); //$NON-NLS-1$
							Cursor normalCursor = new Cursor(Display.getCurrent(),SWT.CURSOR_ARROW);
							parent.setCursor(normalCursor);							
						}
					}
				});

		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(new ISelectionListener() {
					public void selectionChanged(IWorkbenchPart part,
							ISelection selection) {
						// System.out.println("Test!");
					}
				});

		getSite().setSelectionProvider(treeViewer);
		initializeToolBar();
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.dictionarysearch.DictionarySearchView"); //$NON-NLS-1$
	}

	protected String getResultDictionaries(String lemma, String wbbList,
			String suchArt, String limit) throws RemoteException {
		//TODO JaxRpc things
//		Wbb_WebServiceStub webserviceStub = new Wbb_WebServiceStub();
		/*
		 * Wbb_LemmaSearch4RCP2Xml service = new
		 * Wbb_WebServiceStub.Wbb_LemmaSearch4RCP2Xml();
		 * service.setLemma(lemma); service.setWbbList("all");
		 * service.setSuchArt(suchArt); service.setLimit(limit);
		 * webserviceStub._getServiceClient().getOptions().setProperty(
		 * HTTPConstants.CHUNKED, false); Wbb_LemmaSearch4RCP2XmlResponse
		 * response = webserviceStub .wbb_LemmaSearch4RCP2Xml(service);
		 */
		// Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine service = new
		// Wbb_WebServiceStub.Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine();
		// service.setLemma(lemma);
		// service.setLimit(limit);
		// service.setOffset("0");
		// service.setSuchArt(suchArt);
		// service.setWbbList(wbbList);
		//
		// webserviceStub._getServiceClient().getOptions().setProperty(
		// HTTPConstants.CHUNKED, false);
		// Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLineResponse response =
		// webserviceStub
		// .wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine(service);
		// Wbb_WebService wbservice = new Wbb_WebServiceLocator();
		// Wbb_WebServiceSoap service = wbservice.getWbb_WebServiceSoap();
		// String dictResult = service.wbb_LemmaSearch4RCP2Xml(lemma, wbbList,
		// suchArt, limit);
		/*
		 * showMessage("Lemma: " + service.getLemma() + "\n" + "wbbList: " +
		 * service.getWbbList() + "\n" + "suchArt: " + service.getSuchArt() +
		 * "\n" + "limit: " + service.getLimit()); //
		 */
		// showMessage("Hallo hier die Antwort: " + "\n" +
		// response.get_return());
		// return response.get_return();
		return null;
	}
	
	private String getCheckedDictionariesString() {
		Object[] objects = treeViewer.getCheckedElements();
		String result = ""; //$NON-NLS-1$

		for (int i = 0; i < objects.length; i++) {
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView$Library") { //$NON-NLS-1$
				Library lib = (Library) objects[i];
				// System.out.println("Folgende Buecherei wurde ausgewaehlt: "
				// + lib.getName());
			}
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView$DictionaryGroup") { //$NON-NLS-1$
				DictionaryGroup dicgr = (DictionaryGroup) objects[i];
				// System.out.println("Folgende Buechergruppe wurde ausgewaehlt: "
				// + dicgr.getName());
			}
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView$DictionaryW") { //$NON-NLS-1$
				DictionaryW dict = (DictionaryW) objects[i];
				// System.out.println("Woerterbuch: " + dict.getName()
				// + " ausgewaehlt!");
				result = result + dict.getKurz() + ","; //$NON-NLS-1$
			}
		}
		String result2 = ""; //$NON-NLS-1$
		result2 = result.substring(0, result.length() - 1);
		// System.out.println(result2);
		return result2;
	}

	private ArrayList getCheckedDictionaries() {
		ArrayList result = new ArrayList();
		Library lib = null;
		DictionaryGroup dicgr = null;
		DictionaryW dict = null;
		Object[] objects = treeViewer.getCheckedElements();
		for (int i = 0; i < objects.length; i++) {
			result.add(objects[i]);
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchView$Library") { //$NON-NLS-1$
				lib = (Library) objects[i];
				lib.printName();
			}
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchView$DictionaryGroup") { //$NON-NLS-1$
				dicgr = (DictionaryGroup) objects[i];
				dicgr.printName();
			}
			if (objects[i].getClass().getName() == "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchView$Dictionary") { //$NON-NLS-1$
				dict = (DictionaryW) objects[i];
				dict.printName();
			}
		}
		return result;
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(this.getSite().getShell(),
				"Test Search View", message); //$NON-NLS-1$
//treeViewer.getControl().getShell()
	}

	private Object initModel() throws SAXException, IOException,
			ParserConfigurationException {
		if (!checkWebService()) {
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Service Error",  //$NON-NLS-1$
					Messages.DictionarySearchMaskView_Service_Not_Available);
			
			closePerspective();

			return null;
		} else {
			
		
//		Wbb_WebServiceStub stub = new Wbb_WebServiceStub();
//		Wbb_WebServiceStub.Wbb_getAllDictionarys service = new Wbb_WebServiceStub.Wbb_getAllDictionarys();
//		service.setAuthID("1"); //$NON-NLS-1$

//		stub._getServiceClient().getOptions().setProperty(
//				HTTPConstants.CHUNKED, false);
//		String response = stub.wbb_getAllDictionarys("1"); //$NON-NLS-1$
		
		//#############################################################################
		//TODO JaxRpc things	
		//Now the webservice is calling without Axis
		Wbb_WebService_Impl wbservice = new Wbb_WebService_Impl();
		String response = wbservice.getWbb_WebServiceSoap().wbb_getAllDictionarys("1");
//		System.out.println("response: "+response);
			
		//#############################################################################

		DocumentBuilder parser = factory.newDocumentBuilder();

		ErrorHandler handler = new ErrorHandler() {
			public void warning(SAXParseException e) throws SAXException {
				System.err.println("[warning] " + e.getMessage()); //$NON-NLS-1$
			}

			public void error(SAXParseException e) throws SAXException {
				System.err.println("[error] " + e.getMessage()); //$NON-NLS-1$
			}

			public void fatalError(SAXParseException e) throws SAXException {
				System.err.println("[fatal error] " + e.getMessage() + " || " //$NON-NLS-1$ //$NON-NLS-2$
						+ "Column: " + e.getColumnNumber() + "; Line: " //$NON-NLS-1$ //$NON-NLS-2$
						+ e.getLineNumber());
				Date dt = new Date();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss.S"); //$NON-NLS-1$
				long time = System.currentTimeMillis();
				File file = new File("c:/textgrid_errors/" + time + ".txt"); //$NON-NLS-1$ //$NON-NLS-2$
				File xmlFile = new File("c:/textgrid_errors/" + time + ".xml"); //$NON-NLS-1$ //$NON-NLS-2$
				BufferedWriter bw;
				BufferedWriter bwXML;
			}
		};

		parser.setErrorHandler(handler);

		Document document;
		//TODO JaxRpc things
		document = parser.parse(new InputSource(new StringReader(response)));
		// System.out.print(response);
		//TODO JaxRpc things
		NodeList groupNodeList = document.getElementsByTagName("group"); //$NON-NLS-1$
		// get DictionaryGroups
		List libraries = new ArrayList();
		Library library = new Library(Messages.DictionarySearchMaskView_WB_List); //$NON-NLS-1$
		libraries.add(library);
		//TODO JaxRpc things
		for (int i = 0; i < groupNodeList.getLength(); i++) {
			Element group = (Element) groupNodeList.item(i);
			String name = group.getAttribute("name"); //$NON-NLS-1$
			// System.out.println("Groupname: " + name);
			DictionaryGroup dg = new DictionaryGroup(name, library);
			library.addDictionaryGroup(dg);
		}
		//TODO JaxRpc things
		NodeList dictNodeList = document.getElementsByTagName("dict"); //$NON-NLS-1$
		// get Dictionaries
		// System.out.println("Length: " + dictNodeList.getLength());
		
		//TODO JaxRpc things
		for (int i = 0; i < dictNodeList.getLength(); i++) {
			Element dictionary = (Element) dictNodeList.item(i);
			Element parentNode = (Element) dictionary.getParentNode();
			// System.out.println("Parent Node: "
			// + parentNode.getAttribute("name"));
			String name = dictionary.getElementsByTagName("name").item(0) //$NON-NLS-1$
					.getFirstChild().getNodeValue();
			// System.out.println("Name: " + name);
			String kurz = dictionary.getElementsByTagName("kurz").item(0) //$NON-NLS-1$
					.getFirstChild().getNodeValue();
			// System.out.println("Kurz: " + kurz);
			String color = dictionary.getAttribute("color"); //$NON-NLS-1$
			// System.out.println("Color :" + color);
			String bgcolor = dictionary.getAttribute("bgcolor"); //$NON-NLS-1$
			// System.out.println("BGColor :" + bgcolor);
			int index = 1000;
			DictionaryGroup tempgroup = null;

			for (int j = 0; j < library.getDictionaryGroups().size(); j++) {
				tempgroup = (DictionaryGroup) library.getDictionaryGroups()
						.get(j);
				if (tempgroup.getName().equals(parentNode.getAttribute("name"))) { //$NON-NLS-1$
					index = j;
					break;
				}
			}
			// System.out.println("HHHHHH: " + index);
			tempgroup = (DictionaryGroup) library.getDictionaryGroups().get(
					index);
			// System.out.println("BLABLA" + tempgroup.getName());

			DictionaryW tempDict = new DictionaryW(name, kurz, color, bgcolor,
					tempgroup);
			// System.out.println("tempDict.getName() " + tempDict.getName());
			tempgroup.addDictionary(tempDict);

		}
		return libraries;
	}}

	private class Library {
		String name;
		List dictionaryGroups;

		public Library(String name) {
			this.name = name;
			dictionaryGroups = new ArrayList();
		}

		public boolean addDictionaryGroup(DictionaryGroup o) {
			return dictionaryGroups.add(o);
		}

		public List getDictionaryGroups() {
			return dictionaryGroups;
		}

		public String getName() {
			return name;
		}

		public void printName() {
			System.out.println(name);
		}
	}

	private class DictionaryGroup {
		private String name;
		private Library associatedLibrary;
		private List dictionaries;

		public DictionaryGroup(String name, Library associatedLibrary) {
			this.associatedLibrary = associatedLibrary;
			this.name = name;
			dictionaries = new ArrayList();
		}

		public boolean addDictionary(DictionaryW o) {
			return dictionaries.add(o);
		}

		public Library getAssociatedLibrary() {
			return associatedLibrary;
		}

		public List getDictionaries() {
			return dictionaries;
		}

		public String getName() {
			return name;
		}

		public void printName() {
			System.out.println(name);
		}
	}

	private class DictionaryW {
		private String name;
		private String kurz;
		private String color;
		private String bgcolor;
		private DictionaryGroup associatedDictionaryGroup;

		public DictionaryW(String name, String kurz, String color,
				String bgcolor, DictionaryGroup associatedDictionaryGroup) {
			this.associatedDictionaryGroup = associatedDictionaryGroup;
			this.color = color;
			this.bgcolor = bgcolor;
			this.name = name;
			this.kurz = kurz;
		}

		public DictionaryGroup getAssociatedDictionaryGroup() {
			return associatedDictionaryGroup;
		}

		public String getName() {
			return name;
		}

		public String getKurz() {
			return kurz;
		}

		public String getColor() {
			return color;
		}

		public String getBgColor() {
			return bgcolor;
		}

		public void printName() {
			System.out.println(name);
		}

		public boolean getBlockedBackground() {
			// TODO Auto-generated method stub
			return false;
		}

		public void setBlockedBackground() {
			// TODO Auto-generated method stub

		}

		public boolean getBlockedForeground() {
			// TODO Auto-generated method stub
			return false;
		}

		public void setBlockedForeground() {
			// TODO Auto-generated method stub

		}
	}

	@Override
	public void setFocus() {
		if(top != null) 
			top.setFocus();
	}

	private boolean checkAllNecessary() {
		if (lemma.getText().equals("")) { //$NON-NLS-1$
			showMessage(Messages.DictionarySearchMaskView_Specify_Keyword);
			return false;
		}
		if (Integer.parseInt(textLimit.getText()) > 1000) {
			showMessage(Messages.DictionarySearchMaskView_Max_Limit_Value);
			textLimit.setText("1000"); //$NON-NLS-1$
			return false;
		}
		if (getCheckedDictionaries().isEmpty()) {
			showMessage(Messages.DictionarySearchMaskView_Select_Dict);
			return false;
		}
		return true;
	}

	class TestLabelProvider extends LabelProvider implements IFontProvider,
			IColorProvider {

		public Color getBackground(Object element) {
			if (element instanceof DictionaryW) {
				DictionaryW dictionary = (DictionaryW) element;
				if (!dictionary.getBlockedBackground()) {
					Color color = calculateColor(dictionary.getBgColor());
					dictionary.setBlockedBackground();
					return color;
				} else {
					Color color = new Color(null, 238, 238, 238);
					return color;
				}
			}
			return null;
		}

		public Color getForeground(Object element) {
			if (element instanceof DictionaryW) {
				DictionaryW dictionary = (DictionaryW) element;
				if (!dictionary.getBlockedForeground()) {
					Color color = calculateColor(dictionary.getColor());
					dictionary.setBlockedForeground();
					return color;
				} else {
					Color color = new Color(null, 0, 0, 0);
					return color;
				}
			}
			return null;
		}

		public Color calculateColor(String colorString) {
			String stringColor = colorString;
			String substring1 = stringColor.substring(1, 3);
			String substring2 = stringColor.substring(3, 5);
			String substring3 = stringColor.substring(5, 7);
			int redValue = Integer.parseInt(substring1, 16);
			int greenValue = Integer.parseInt(substring2, 16);
			int blueValue = Integer.parseInt(substring3, 16);

			Color color = new Color(null, redValue, greenValue, blueValue);
			return color;
		}

		@Override
		public Image getImage(Object element) {
			return null;
		}

		@Override
		public String getText(Object element) {
			if (element instanceof Library) {
				Library library = (Library) element;
				return library.getName();
			} else if (element instanceof DictionaryGroup) {
				DictionaryGroup dictionaryGroup = (DictionaryGroup) element;
				return dictionaryGroup.getName();
			} else if (element instanceof DictionaryW) {
				DictionaryW dictionary = (DictionaryW) element;
				return dictionary.getName();
			} else
				return null;
		}

		@Override
		public void addListener(ILabelProviderListener listener) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
		}

		public Font getFont(Object element) {
			if (element instanceof DictionaryW) {
				DictionaryW dict = (DictionaryW) element;
				Font returnValue = null;
				Font defaultFont = JFaceResources.getDefaultFont();
				FontData[] data = defaultFont.getFontData();
				for (int i = 0; i < data.length; i++) {
					data[i].setStyle(SWT.BOLD);
				}
				returnValue = new Font(Display.getDefault(), data);
				return returnValue;
			}
			return null;
		}
	}

	private void initializeToolBar() {
		IToolBarManager toolBarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}
	
	private void closePerspective() {
		IWorkbench wb = PlatformUI.getWorkbench();
		wb.getActiveWorkbenchWindow().getActivePage().closePerspective(wb.getPerspectiveRegistry().findPerspectiveWithId(
				"info.textgrid.lab.woerterbuchsearchmask.perspectives.RelEngPerspective"), false, true); //$NON-NLS-1$
	}
	
	public boolean checkWebService() {
        String URL = TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER;
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(URL).openConnection();
            con.setRequestMethod("GET"); //$NON-NLS-1$
            con.setReadTimeout(READ_TIME_OUT);
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            System.out.println("FEHLER!!!!!!!!!!!"); //$NON-NLS-1$
            return false;
        }
    }
    
}
