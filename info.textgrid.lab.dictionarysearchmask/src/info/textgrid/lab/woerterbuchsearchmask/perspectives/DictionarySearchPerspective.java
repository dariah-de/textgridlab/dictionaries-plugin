/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package info.textgrid.lab.woerterbuchsearchmask.perspectives;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * This class is meant to serve as an example for how various contributions are
 * made to a perspective. Note that some of the extension point id's are
 * referred to as API constants while others are hardcoded and may be subject to
 * change.
 */
public class DictionarySearchPerspective implements IPerspectiveFactory {

	public DictionarySearchPerspective() {
		super();
	}

	/**
	 * Creates the initial layout for a page.
	 */
	public void createInitialLayout(IPageLayout layout) {
		layout.setEditorAreaVisible(false);
		String editorArea = layout.getEditorArea();
		addFastViews(layout);
		addViewShortcuts(layout);
		addPerspectiveShortcuts(layout);
		layout.addView("info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView", IPageLayout.TOP, 0.5f, IPageLayout.ID_EDITOR_AREA);
		//layout.addView("info.textgrid.lab.dictionarysearch.views.DictionaryBrowserView", IPageLayout.BOTTOM, 0.2f, IPageLayout.ID_EDITOR_AREA);
		layout.addView("info.textgrid.lab.dictionarysearch.views.DictionarySearchView", IPageLayout.RIGHT, 0.57f, "info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView");
	}


	/**
	 * Add fast views to the perspective.
	 */
	private void addFastViews(IPageLayout layout) {
	}

	/**
	 * Add view shortcuts to the perspective.
	 */
	private void addViewShortcuts(IPageLayout layout) {
	}

	/**
	 * Add perspective shortcuts to the perspective.
	 */
	private void addPerspectiveShortcuts(IPageLayout layout) {
	}

}
