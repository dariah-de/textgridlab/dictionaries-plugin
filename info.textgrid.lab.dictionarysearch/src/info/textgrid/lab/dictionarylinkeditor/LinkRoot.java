package info.textgrid.lab.dictionarylinkeditor;

import info.textgrid.lab.dictionarylinkeditor.views.DictionaryLinkEditor;
import info.textgrid.lab.dictionarysearch.Activator;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebService_Impl;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

//import org.apache.axis2.AxisFault;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * This object holds a WritableList of WBLinks and knows how to retrieve all
 * WBLinks given a source Lemma and to add a link. Will be used by the
 * Dictionary Link Editor.
 * 
 * @author martin
 * 
 */
public class LinkRoot extends PlatformObject {
	private ArrayList<WBLink> wbLinks;
	private WritableList writeList;
	private ArrayList<WBLink> reserveList;
	private ArrayList<WBLink> manualList;
	private WBLemma sourceLemma;
	//TODO JaxRpc things
//	private Wbb_WebServiceStub stub;
	private DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();

	public LinkRoot() {
		wbLinks = new ArrayList<WBLink>();
		writeList = new WritableList(wbLinks, WBLink.class);
		reserveList = new ArrayList<WBLink>();
		manualList = new ArrayList<WBLink>();
		//TODO JaxRpc things
//		try {
//			stub = new Wbb_WebServiceStub();
//			stub._getServiceClient().getOptions().setProperty(
//					HTTPConstants.CHUNKED, false);
//		} catch (AxisFault e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not initialize Wbb stub");
//		}
	}

	/**
	 * Set source lemma and fill the model with WBLinks.
	 * 
	 * @param sourceLemma
	 */
	public LinkRoot(WBLemma sourceLemma) {
		this();
		this.sourceLemma = sourceLemma;
		retrieveLinks();
	}

	/**
	 * retrieves WBLinks from Wbb for the set sourceLemma
	 */
	public void retrieveLinks() {
		if (sourceLemma == null)
			return;
		// call Michael's service now.
		// Wbb_getLinks wblrequest = new Wbb_getLinks();
		// wblrequest.setSourceid(sourceLemma.getID());
		clearLinks();
		String enclosure = "";
		//TODO JaxRpc things
//		try {
//			enclosure = stub.wbb_getLinks(sourceLemma.getWbShortName()+"#"+sourceLemma.getID());
////			enclosure = stub.wbb_getLinks(sourceLemma.getID());
//		} catch (RemoteException e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not retrieve links from Wbb");
//		}
		
		//#####################################################################################
		
		Wbb_WebService_Impl wbservice = new Wbb_WebService_Impl();
		try {
			enclosure = wbservice.getWbb_WebServiceSoap().wbb_getLinks(sourceLemma.getWbShortName()+"#"+sourceLemma.getID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not retrieve links from Wbb");
		}
		
		//#####################################################################################
		
		if (DictionaryLinkEditor.DEBUG_OUTPUT) {
			System.out.println("enclosure back: " + enclosure);
		}
		parseResult(enclosure); // will fill wbLinks ArrayList

		addPreviouslyManuallyAddedLinks();
	}

	private void addPreviouslyManuallyAddedLinks() {
		for (WBLink l : manualList) {
			WBLinkGroup.setEditable(l.getSourceLemma().getID(), l
					.getTargetLemma().getID(), true);
		}
		writeList.addAll(manualList);
	}

	public boolean removeFromUnprocessedList(WBLink w) {
		return manualList.remove(w);
	}

	public void parseResult(String enclosure) {

		ErrorHandler handler = new ErrorHandler() {
			public void warning(SAXParseException e) throws SAXException {
				System.err.println("[warning] " + e.getMessage());
			}

			public void error(SAXParseException e) throws SAXException {
				System.err.println("[error] " + e.getMessage());
			}

			public void fatalError(SAXParseException e) throws SAXException {
				System.err.println("[fatal error] " + e.getMessage() + " || "
						+ "Column: " + e.getColumnNumber() + "; Line: "
						+ e.getLineNumber());
			}
		};

		Document document = null;
		try {
			DocumentBuilder parser = factory.newDocumentBuilder();
			parser.setErrorHandler(handler);
			document = parser
					.parse(new InputSource(new StringReader(enclosure)));
		} catch (ParserConfigurationException e) {
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not build parser for Wbb");
		} catch (SAXException e) {
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not parse result from Wbb");
		} catch (IOException e) {
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not retrieve content from Wbb");
		}

		NodeList targetLinkNodeList = document.getElementsByTagName("link");
		int linkNumber = targetLinkNodeList.getLength();
		// System.out.println("Found " + linkNumber + " links.");
		for (int i = 0; i < linkNumber; i++) {
			Element link = (Element) targetLinkNodeList.item(i);
			// System.out.println("Looking at Link number " + i + " which is: "
			// + link.getNodeValue());
			Element lemmaNode = (Element) link.getElementsByTagName("lemma")
					.item(0);
			WBLemma targetLemma = parseLemmaNode(lemmaNode);
			WBLink wblink = parseLink(sourceLemma, targetLemma, link);

			boolean isEditable = WBLinkGroup.setEditable(sourceLemma.getID(),
					targetLemma.getID(), wblink.isEditable());
			if (!isEditable) {
				Activator.handleProblem(IStatus.WARNING, null,
						"Conflict in links from " + sourceLemma.getID()
								+ " to " + targetLemma.getID() + "("
								+ targetLemma.getName()
								+ "): some are editable, some not!");
				System.out.println("Conflict in links from "
						+ sourceLemma.getID() + " to " + targetLemma.getID()
						+ "(" + targetLemma.getName()
						+ "): some are editable, some not!");
			}
			WBLinkGroup.isLastOne(sourceLemma.getID(), targetLemma.getID(),
					wblink.getTimestamp());
			writeList.add(wblink);
		}
	}

	public WBLink parseLink(WBLemma source, WBLemma target, Element el) {
		// System.out.println("Want to parse the link...");

		String editor = "";
		Node n = el.getElementsByTagName("editor").item(0).getFirstChild();
		if (n != null)
			editor = n.getNodeValue();

		String time = "";
		n = el.getElementsByTagName("timestamp").item(0).getFirstChild();
		if (n != null)
			time = n.getNodeValue();

		String comment = "";
		n = el.getElementsByTagName("comment").item(0).getFirstChild();
		if (n != null)
			comment = n.getNodeValue();
		try {
			comment = URLDecoder.decode(comment, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}

		String approved = "";
		n = el.getElementsByTagName("approved").item(0).getFirstChild();
		if (n != null)
			approved = n.getNodeValue();

		String deleted = "";
		n = el.getElementsByTagName("deleted").item(0).getFirstChild();
		if (n != null)
			deleted = n.getNodeValue();

		String editable = "";
		n = el.getElementsByTagName("editable").item(0).getFirstChild();
		if (n != null)
			editable = n.getNodeValue();

		String etymRelationId = ((Element) el.getElementsByTagName(
				"etymrelation").item(0)).getAttribute("id");

		String semanticRelationId = ((Element) el.getElementsByTagName(
				"semanticrelation").item(0)).getAttribute("id");

		String quality = ((Element) el.getElementsByTagName("quality").item(0))
				.getAttribute("id");

		WBLink wbl = new WBLink(source, target, editor, time, quality,
				semanticRelationId, etymRelationId, comment, approved, deleted,
				editable);
		return wbl;
	}

	public WBLemma parseLemmaNode(Element ln) {
		String name = ln.getElementsByTagName("name").item(0).getFirstChild()
				.getNodeValue();
		String wbname = ln.getElementsByTagName("wbname").item(0)
				.getFirstChild().getNodeValue();
		String wbshort = ln.getElementsByTagName("wbkurz").item(0)
				.getFirstChild().getNodeValue();
		String bg = ln.getElementsByTagName("wbkurz").item(0).getAttributes()
				.getNamedItem("bgcolor").getTextContent();
		String fg = ln.getElementsByTagName("wbkurz").item(0).getAttributes()
				.getNamedItem("color").getTextContent();
		NodeList hrefNode = ln.getElementsByTagName("href");
		// if (hrefNode == null) {
		// // give this a try... but does not save it as lhref's are forbidden
		// in DTD
		// hrefNode = ln.getElementsByTagName("lhref");
		// }
		String href = hrefNode.item(0).getFirstChild().getNodeValue();
		// System.out.println("One Target Lemma: " + name);
		WBLemma l = new WBLemma(name, wbname, wbshort, bg, fg, href);
		return l;
	}

	public WBLemma getSourceLemma() {
		return sourceLemma;
	}

	/**
	 * will retrieve or clear all links depending on whether sourceLemma is set
	 * or null.
	 * 
	 * @param sourceLemma
	 */
	public void setSourceLemma(WBLemma sourceLemma) {
		this.sourceLemma = sourceLemma;
		this.manualList.clear();
		clearLinks();
		if (sourceLemma != null) {
			retrieveLinks();
		}
	}

	/**
	 * clears all lists (the displayed one, the reserve list, and the LinkGroup
	 * registry).
	 * 
	 */
	public void clearLinks() {
		writeList.clear();
		reserveList = new ArrayList<WBLink>();
		WBLinkGroup.resetRegistry();
	}

	/**
	 * As we have a WritableList, we do not need a filter. This method uses a
	 * reserve List to write all non-final links to that should not occur in the
	 * viewer. This is fast enough - O(n) - and works.
	 * 
	 * @param on
	 */
	public void toggleHistory(boolean on) {
		if (DictionaryLinkEditor.DEBUG_OUTPUT) {
			System.out.println("toggling..." + on);
		}
		if (on) {
			for (WBLink wbl : reserveList) {
				writeList.add(wbl);
			}
			reserveList = new ArrayList<WBLink>();
		} else {
			for (Object o : writeList) {
				WBLink wbl = (WBLink) o;
				if (!wbl.isLastInHistory()) {
					// System.out
					// .println("sorting out "
					// + wbl.getTargetLemma().getName()
					// + wbl.getTargetLemma().getID()
					// + wbl.getTimestamp());
					reserveList.add(wbl);
				} else {
					// System.out
					// .println("keeping : "
					// + wbl.getTargetLemma().getName()
					// + wbl.getTargetLemma().getID()
					// + wbl.getTimestamp());
				}
			}
			for (WBLink wbl : reserveList) {
				writeList.remove(wbl);
			}
		}
	}

	public WritableList getList() {
		return writeList;
	}

	/**
	 * Add a manually selected Link to the List by specifying its target lemma.
	 * Returns the WBLink just created with defaults for manual links, or null
	 * if this source-target lemma combination is already not editable anymore.
	 * 
	 * @param targetLemma
	 * @return
	 */
	public WBLink addNewLink(WBLemma targetLemma) {
		WBLink newLink = new WBLink(sourceLemma, targetLemma);
		WBLinkGroup.setEditable(sourceLemma.getID(), targetLemma.getID(), true);
		WBLinkGroup.isLastOne(sourceLemma.getID(), targetLemma.getID(),
				WBLink.UNSAVED_TIMESTAMP);
		writeList.add(newLink);
		manualList.add(newLink);
		return newLink;
	}
}
