package info.textgrid.lab.dictionarylinkeditor;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.PlatformObject;

public class WBLinkGroup extends PlatformObject {

	private static Map<String, Map<String, GroupAttributes>> registry = new HashMap<String, Map<String, GroupAttributes>>();

	/**
	 * Enters a link form SourceID to TargetID into the registry. All Links
	 * within a LinkGroup must be either all editable or all non-editable. This
	 * will return true if there was no conflict.
	 * 
	 * @param sourceID
	 * @param targetID
	 * @param editable
	 * @return
	 */
	public static boolean setEditable(String sourceID, String targetID,
			boolean editable) {
		// System.out.println("grouping: "+ sourceID + targetID + editable);

		boolean noConflict = true;

		if (registry.containsKey(sourceID)) {
			Map<String, GroupAttributes> targets = registry.get(sourceID);
			if (targets.containsKey(targetID)) {
				// System.out.println("found it");
				if (editable != targets.get(targetID).isEditable()) {
					noConflict = false;
					// System.out.println("conflict!");
				}
			} else {
				targets.put(targetID, new GroupAttributes(editable));
				// System.out.println("put it");
			}
		} else {
			Map<String, GroupAttributes> target = new HashMap<String, GroupAttributes>();
			target.put(targetID, new GroupAttributes(editable));
			registry.put(sourceID, target);
			// System.out.println("put new source");
		}
		return noConflict;
	}

	public static void resetRegistry() {
		registry = new HashMap<String, Map<String, GroupAttributes>>();
	}

	/**
	 * compares the timestamp in the registry with the argument. returns true
	 * and sets the one in the registry to be the argument if the argument is
	 * later.
	 * 
	 * Assumes the registry has been filled already by the setEditable function.
	 * 
	 * @param sourceID
	 * @param targetID
	 * @param timestamp
	 * @return
	 */
	public static boolean isLastOne(String sourceID, String targetID,
			String timestamp) {
		// Assume the source-target mapping is already present
		GroupAttributes ga = registry.get(sourceID).get(targetID);
		if (ga.getLastTimestamp().compareTo(timestamp) <= 0) {
			ga.setLastTimestamp(timestamp);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * true if this source-target combination is already present in the
	 * registry.
	 * 
	 * @param sourceID
	 * @param targetID
	 * @return
	 */
	public static boolean isInRegistry(String sourceID, String targetID) {
		boolean result = false;
		if (registry.containsKey(sourceID)) {
			if (registry.get(sourceID).containsKey(targetID)) {
				result = true;
			}
		}
		return result;
	}
}
