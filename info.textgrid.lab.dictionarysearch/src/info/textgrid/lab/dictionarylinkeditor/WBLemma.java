package info.textgrid.lab.dictionarylinkeditor;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

/**
 * This is the representation of the Lemma of a Dictionary (WB). Will be used by
 * WBLink as source or target values.
 * 
 * @author martin
 * 
 */
public class WBLemma extends PlatformObject {
	private String name;
	private String wbName;
	private String wbShortName;
	private Color wbBgColor;
	private Color wbFgColor;
	private String wbBgColorString;
	private String wbFgColorString;
	private String href;
	private String ID;

	public WBLemma(String name, String wbName, String wbShortName,
			String wbBgColor, String wbFgColor, String href) {
		this.name = name;
		// System.out.println("Lemma "+name);
		this.wbName = wbName;
		//System.out.println("Initialised Lemma: "+ this.name);
		this.wbShortName = wbShortName;
		this.wbBgColorString = wbBgColor;
		this.wbFgColorString = wbFgColor;
		this.href = href;

		// TODO: check whether this works in all circumstances
		this.ID = href.substring(href.indexOf("bookref=") + 8);
		//this.ID = href.substring(href.indexOf("lemid=") + 6);
		//System.out.println("...has ID: " + this.ID);
	}

	public String getName() {
		return name;
	}

	public String getWbName() {
		return wbName;
	}

	public String getWbShortName() {
		return wbShortName;
	}

	public Color getWbBgColor() {
		String stringColor = wbBgColorString;
		String substring1 = stringColor.substring(1, 3);
		String substring2 = stringColor.substring(3, 5);
		String substring3 = stringColor.substring(5, 7);
		int redValue = Integer.parseInt(substring1, 16);
		int greenValue = Integer.parseInt(substring2, 16);
		int blueValue = Integer.parseInt(substring3, 16);
		this.wbBgColor = new Color(null, redValue, greenValue, blueValue);
		return wbBgColor;
	}

	public Color getWbFgColor() {
		String stringColor = wbFgColorString;
		String substring1 = stringColor.substring(1, 3);
		String substring2 = stringColor.substring(3, 5);
		String substring3 = stringColor.substring(5, 7);
		int redValue = Integer.parseInt(substring1, 16);
		int greenValue = Integer.parseInt(substring2, 16);
		int blueValue = Integer.parseInt(substring3, 16);
		this.wbFgColor = new Color(null, redValue, greenValue, blueValue);
		return wbFgColor;
	}

	public String getHref() {
		return href;
	}

	public String getID() {
		return ID;
	}

}
