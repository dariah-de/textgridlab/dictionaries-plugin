package info.textgrid.lab.dictionarylinkeditor.views;

import info.textgrid.lab.dictionarylinkeditor.WBLink;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;

/**
 * Adds editing support to the List: selected (a CheckBox), relations (two
 * Combos) and comment (Free Text).
 * 
 * @author martin
 * 
 */
public class WBLinkEditingSupport extends EditingSupport {
	private CellEditor editor;
	private int column;

	public WBLinkEditingSupport(ColumnViewer viewer, int column) {
		super(viewer);

		String[] semanticRelations = DictionaryLinkEditor.semanticRelations;
		String[] etymRelations = DictionaryLinkEditor.etymRelations;

		// Create the correct editor based on the column index
		switch (column) {
		case 0:
			editor = new CheckboxCellEditor(null, SWT.CHECK);
			break;
		case 5:
			editor = new ComboBoxCellEditor(((TableViewer) viewer).getTable(),
					semanticRelations);
			break;
		case 6:
			editor = new ComboBoxCellEditor(((TableViewer) viewer).getTable(),
					etymRelations);
			break;
		case 8:
			editor = new TextCellEditor(((TableViewer) viewer).getTable());
			break;
		default:
			editor = null;
		}
		this.column = column;
	}

	@Override
	protected boolean canEdit(Object element) {
		WBLink wbl = (WBLink) element;
		if (wbl.immutable(DictionaryLinkEditor.whoIsLoggedIn)) {
			return false;
		}
		return true;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return editor;
	}

	@Override
	protected Object getValue(Object element) {
		WBLink wbl = (WBLink) element;
		switch (this.column) {
		case 0:
			return wbl.isTouched();
		case 1:
			return null;
		case 2:
			return wbl.getTargetLemma().getName();
		case 3:
			return wbl.getTargetLemma().getWbShortName();
		case 4:
			return wbl.getQualityStringFromID();
		case 5:
			return wbl.getSemanticRelationID();
		case 6:
			return wbl.getEtymRelationID();
		case 7:
			return wbl.getEditor();
		case 8:
			return wbl.getComment();
		default:
			break;
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		WBLink wbl = (WBLink) element;
		switch (this.column) {
		case 0:
			if (!wbl.immutable(DictionaryLinkEditor.whoIsLoggedIn))
				wbl.touch((Boolean) value);
			break;
		case 5:
			wbl.setSemanticRelationID((Integer) value);
			break;
		case 6:
			wbl.setEtymRelationID((Integer) value);
			break;
		case 8:
			wbl.setComment((String) value);
		default:
			break;
		}
		getViewer().update(element, null);
	}
}
