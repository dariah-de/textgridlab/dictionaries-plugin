package info.textgrid.lab.dictionarylinkeditor.views;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.dictionarylinkeditor.LinkRoot;
import info.textgrid.lab.dictionarylinkeditor.WBLemma;
import info.textgrid.lab.dictionarylinkeditor.WBLink;
import info.textgrid.lab.dictionarylinkeditor.WBLinkGroup;
import info.textgrid.lab.dictionarysearch.Activator;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebService_Impl;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.text.Collator;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

//import org.apache.axis2.AxisFault;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.swtdesigner.SWTResourceManager;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Group;

/**
 * Dictionary Link Editor View. Will be opened and filled from the
 * DictionarySearchView by clicking the link/nolink symbol (@/--). The first
 * click on the symbol opens this View and sets the Source Lemma, i.e. all Links
 * starting from it having some target. On the second and further clicks on
 * other lemmas, those lemmas will be inserted as new targets constituting new
 * links.
 * 
 * Whenever changed links are to be made permanent, authentication is mandatory.
 * 
 * When a change will be made permanent, a new link reflecting the new status
 * will be inserted such that the history of changes can be reconstructed.
 * 
 * Allowed changes are a function of:
 * <ul>
 * <item>user status: WBL_ADMIN in the WBL_AUTHZ_PROJECT, WBL_EDITOR, or other
 * including not authenticated (in this order)</item>
 * 
 * <item>The Link's quality tag (1-3 Book/inferred from Book), 4 manually set by
 * this DictionaryLinkEditor, 5+ computed. This is read-only.</item>
 * 
 * <item>approved flag: 0 initially computed, 1 Book, 2 approved by editor, 3
 * approved by admin, 9 marked doubtful</item>
 * 
 * <item>deleted flag: 1 true or 0 false </item>
 * 
 * <item>editable flag: 1 true or 0 false. All Links from LemmaA/DictX to
 * LemmaB/DictY in the link history SHOULD be either all editable or all not
 * editable. If not, the server did not set them correctly.</item>
 * 
 * <item>touchedness: when a relation or comment was changed or a new link to
 * some new target inserted leading to selection of these links.</item>
 * 
 * <item>the operation to be acted upon the link: delete, set/approve, mark
 * doubtful</item>
 * </ul>
 * 
 * History toggling is not in the ContentProvider but in the model, see
 * LinkRoot.
 * 
 * @author martin
 */

public class DictionaryLinkEditor extends ViewPart {
	public static final String WBL_AUTHZ_PROJECT = "TGPR188";
	public static final String WBL_EDITOR = TextGridProject.TG_STANDARD_ROLE_EDITOR;
	public static final String WBL_ADMIN = TextGridProject.TG_STANDARD_ROLE_ADMINISTRATOR;
	public static final boolean DEBUG_OUTPUT = false;
	public static String whoIsLoggedIn = "";
	// might be "" or WBL_EDITOR or WBL_ADMIN
	private TableViewer viewer;
	private WBLemma sourceLemma;
	private Action action1;
	private Action action2;
	private boolean targetState = false;
	private boolean showHistory = false;
	private LinkRoot linkRoot;
	//TODO JaxRpc things
//	private Wbb_WebServiceStub stub;
	private Wbb_WebService_Impl wbservice = null;
	public static String[] semanticRelations;
	public static String[] etymRelations;
	private Composite topBar;
	private Composite bottomBar;
	private Label sourceLabel;
	private Label sourceWBLabel;
	private Label topHint;
	private Label toggleHistoryLabel;
	private Label finishLabel;
	private Label finishLabel2;
	private int failedToWrite = 0;
	private static Image CHECKED;
	private static Image UNCHECKED;
	private static Image IMMUTABLE;
	private static Image NEWLINK;
	private static Image MANUALLINK;
	private static Image COMPUTEDLINK;
	public static final int MANUALORCOMPUTEDAPPROVEDID = 0;
	private static Image BOOKLINK;
	public static final int BOOKAPPROVEDID = 1;
	private static Image EDITORLINK;
	public static final int EDITORAPPROVEDID = 2;
	private static Image ADMINLINK;
	public static final int ADMINAPPROVEDID = 3;
	private static Image DOUBTLINK;
	public static final int DOUBTAPPROVEDID = 9;
	private static Image DELETEDLINK;
	private Button setButton;
	private Button doubtButton;
	private Button historyButton;
	private Button finishButton;
	private Button cancelButton;
	private Button deleteButton;
	private boolean enableButtons = false;
	private ISIDChangedListener sidChangedListener;
	private SelectionListener disableSelectionListener;
	private Group group;
	
	
	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			WBLink wbl = (WBLink) obj;

			if (index == 0) {
				// selected
				return null;
			} else if (index == 1) {
				// approved status
				return null;
			} else if (index == 2) {
				return wbl.getTargetLemma().getName();
			} else if (index == 3) {
				return wbl.getTargetLemma().getWbShortName();
			} else if (index == 4) {
				return wbl.getQualityStringFromID();
			} else if (index == 5) {
				if (wbl.getSemanticRelationID() == -1)
					return "<none>";
				else if (wbl.getSemanticRelationID() >= 0
						&& wbl.getSemanticRelationID() < semanticRelations.length)
					return semanticRelations[wbl.getSemanticRelationID()];
				else
					return "<unknown>";
			} else if (index == 6) {
				if (wbl.getEtymRelationID() == -1)
					return "<none>";
				else if (wbl.getEtymRelationID() >= 0
						&& wbl.getEtymRelationID() < etymRelations.length)
					return etymRelations[wbl.getEtymRelationID()];
				else
					return "<unknown>";
			} else if (index == 7) {
				return wbl.getEditor();
			} else if (index == 8) {
				return wbl.getComment();
			} else
				return "extra column " + index;
		}

		public Image getColumnImage(Object obj, int index) {
			if (index == 0) {
				WBLink wbl = (WBLink) obj;
				if (wbl.immutable(whoIsLoggedIn)) {
					return IMMUTABLE;
				} else {
					if (wbl.isTouched()) {
						return CHECKED;
					} else {
						return UNCHECKED;
					}
				}
			} else if (index == 1) {
				WBLink wbl = (WBLink) obj;
				if (wbl.isDeleted()) {
					return DELETEDLINK;
				} else if (wbl.isManuallyAdded()) {
					//unsaved yet
					return NEWLINK;
				} else {
					switch (wbl.getApproved()) {
					case MANUALORCOMPUTEDAPPROVEDID:
						if (wbl.getQualityID() == 4)
							return MANUALLINK;
						else
							return COMPUTEDLINK;
					case BOOKAPPROVEDID:
						return BOOKLINK;
					case EDITORAPPROVEDID:
						return EDITORLINK;
					case ADMINAPPROVEDID:
						return ADMINLINK;
					case DOUBTAPPROVEDID:
						return DOUBTLINK;
					default:
						return null;
					}
				}
			} else {
				return null;// getImage(obj);
			}
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	/**
	 * Sort a WBLink like this: first Target Lemma name, then Target Lemma
	 * dictionary short name, then timestamp. Uses a Collator to ensure that the
	 * String is sorted right (a á Ba bi c).
	 * 
	 * @author martin
	 * 
	 */
	class NameTimeSorter extends ViewerSorter {
		Collator col = Collator.getInstance();

		public int compare(Viewer viewer, Object o1, Object o2) {
			WBLink wbl1 = (WBLink) o1;
			WBLink wbl2 = (WBLink) o2;

			int level1 = col.compare(wbl1.getTargetLemma().getName(), wbl2
					.getTargetLemma().getName());
			if (level1 != 0)
				return level1;
			else {
				int level2 = col.compare(
						wbl1.getTargetLemma().getWbShortName(), wbl2
								.getTargetLemma().getWbShortName());
				if (level2 != 0)
					return level2;
				else {
					return wbl1.getTimestamp().compareTo(wbl2.getTimestamp());
				}
			}
		}
	}

	/**
	 * The constructor.
	 */
	public DictionaryLinkEditor() {
		try {
			CHECKED = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/checked.gif"), null))).createImage();
			UNCHECKED = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/unchecked.gif"), null))).createImage();
			IMMUTABLE = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/grey.gif"), null))).createImage();
			NEWLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/new.png"), null))).createImage();
			MANUALLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/manual.png"), null))).createImage();
			COMPUTEDLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/computed.png"), null))).createImage();
			BOOKLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/book.png"), null))).createImage();
			EDITORLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/editor.png"), null))).createImage();
			ADMINLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/admin.png"), null))).createImage();
			DOUBTLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/doubt.png"), null))).createImage();
			DELETEDLINK = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path(
							"icons/deleted.png"), null))).createImage();
		} catch (IOException e1) {
			Activator.handleProblem(IStatus.ERROR, e1, "Could not find icons");
		}

		//TODO JaxRpc things
//		try {
//			stub = new Wbb_WebServiceStub();
//			stub._getServiceClient().getOptions().setProperty(
//					HTTPConstants.CHUNKED, false);
//		} catch (AxisFault e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not initialize stub for wbb");
//		}
		
		//#####################################################################################
		try {
			wbservice = new Wbb_WebService_Impl();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not initialize webservice for wbb");
		}
				
		//#####################################################################################
		

		Document semdocument = null;
		Document etymdocument = null;
		//TODO JaxRpc things
//		try {
//			String semenclosure = stub.wbb_getAllSemanticRelations();
//			String etymenclosure = stub.wbb_getAllEtymRelations();
//			// System.out.println("semenclosure back: " + semenclosure);
//			// System.out.println("etymenclosure back: " + etymenclosure);
//			final DocumentBuilderFactory factory = DocumentBuilderFactory
//					.newInstance();
//			DocumentBuilder parser = factory.newDocumentBuilder();
//			semdocument = parser.parse(new InputSource(new StringReader(
//					semenclosure)));
//			etymdocument = parser.parse(new InputSource(new StringReader(
//					etymenclosure)));
//		} catch (RemoteException e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not retrieve relation names from Wbb");
//		} catch (ParserConfigurationException e) {
//			Activator.handleProblem(IStatus.ERROR, e, "Could not build parser");
//		} catch (SAXException e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not parse result from wbb");
//		} catch (IOException e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not find result from wbb");
//		}
		
		
		//#####################################################################################
		
		try {
			String semenclosure = wbservice.getWbb_WebServiceSoap().wbb_getAllSemanticRelations();
			String etymenclosure = wbservice.getWbb_WebServiceSoap().wbb_getAllEtymRelations();
		
			final DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder parser = factory.newDocumentBuilder();
			semdocument = parser.parse(new InputSource(new StringReader(
					semenclosure)));
			etymdocument = parser.parse(new InputSource(new StringReader(
					etymenclosure)));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
			"Could not retrieve relation names from Wbb");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
					"Could not parse result from wbb");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e, "Could not build parser");
		}
		
		//#####################################################################################
		
		NodeList semrelationNodeList = semdocument
				.getElementsByTagName("relation");
		int semrelationNumber = semrelationNodeList.getLength();
		// System.out.println("Found " + semrelationNumber + " semrelations.");
		semanticRelations = new String[semrelationNumber];
		for (int i = 0; i < semrelationNumber; i++) {
			Element r = (Element) semrelationNodeList.item(i);
			String id = r.getAttribute("id");
			Integer idInteger = new Integer(id);
			if (i != idInteger) {
				System.err
						.println("Warning: Relation Ids do not start with 0 (found Id "
								+ idInteger + " at index " + i);
			}
			String relName = r.getFirstChild().getNodeValue();
			semanticRelations[i] = relName;
			// System.out.println("Just put " + id + " named " + relName);
		}
		NodeList etymrelationNodeList = etymdocument
				.getElementsByTagName("relation");
		int etymrelationNumber = etymrelationNodeList.getLength();
		// System.out.println("Found " + etymrelationNumber + "
		// etymrelations.");
		etymRelations = new String[etymrelationNumber];
		for (int i = 0; i < etymrelationNumber; i++) {
			Element r = (Element) etymrelationNodeList.item(i);
			String id = r.getAttribute("id");
			Integer idInteger = new Integer(id);
			if (i != idInteger) {
				System.err
						.println("Warning: Relation Ids do not start with 0 (found Id "
								+ idInteger + " at index " + i);
			}
			String relName = r.getFirstChild().getNodeValue();
			etymRelations[i] = relName;
			// System.out.println("Just put " + id + " named " + relName);
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {

		findOutWhoIsLoggedIn(RBACSession.getInstance().getSID(false));

		sidChangedListener = new ISIDChangedListener() {
			public void sidChanged(String newSID, String newEPPN) {
				findOutWhoIsLoggedIn(newSID);
				if (linkRoot != null) {
					linkRoot.retrieveLinks();
					linkRoot.toggleHistory(showHistory);
					setButtonsEnabled(enableButtons);
				}
			}
		};
		AuthBrowser.addSIDChangedListener(sidChangedListener);

		linkRoot = new LinkRoot();
		Composite c = new Composite(parent, SWT.FILL);
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		cGD.heightHint = 100;
		cGD.widthHint = 400;
		cGD.verticalAlignment = GridData.FILL;
		c.setLayout(new GridLayout(1, false));
//		c.setLayoutData(cGD);

		topBar = new Composite(c, SWT.FILL);
		GridLayout gridLayout = new GridLayout(5, false);
		gridLayout.horizontalSpacing = 6;
		topBar.setLayout(gridLayout);
		GridDataFactory.fillDefaults().applyTo(topBar);

		Label sourceLabelTitle = new Label(topBar, SWT.NONE);
		sourceLabelTitle.setText("Source Lemma");
		sourceLabel = new Label(topBar, SWT.BORDER);
		sourceLabel.setToolTipText("");
		sourceLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));

		Font defaultFont = JFaceResources.getDefaultFont();
		FontData[] fdatab = defaultFont.getFontData();
		for (int i = 0; i < fdatab.length; i++) {
			fdatab[i].setStyle(SWT.BOLD);
		}
		sourceLabel.setFont(new Font(null, fdatab));
		sourceLabel.setText("");
		sourceWBLabel = new Label(topBar, SWT.NONE);
		sourceWBLabel.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD));
		sourceWBLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		sourceWBLabel.setToolTipText("");
		sourceWBLabel.setText("");
		topHint = new Label(topBar, SWT.NONE);
		topHint.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLUE));
		topHint.setText("");
		
		group = new Group(topBar, SWT.SHADOW_ETCHED_IN);
		GridLayout gridLayout_1 = new GridLayout(3, false);
		gridLayout_1.verticalSpacing = 1;
		gridLayout_1.marginWidth = 1;
		gridLayout_1.marginHeight = 1;
		gridLayout_1.horizontalSpacing = 1;
		group.setLayout(gridLayout_1);
		{
			GridData gridData = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
			gridData.heightHint = 25;
			gridData.widthHint = 238;
			group.setLayoutData(gridData);
		}
				
						historyButton = new Button(group, SWT.TOGGLE);
						GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
						gridData.heightHint = 16;
						historyButton.setLayoutData(gridData);
						historyButton.setToolTipText("You can show/hide the change history, if available.");
						historyButton.setText("Show/Hide");
						new Label(group, SWT.NONE);
						toggleHistoryLabel = new Label(group, SWT.NONE);
						toggleHistoryLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
						toggleHistoryLabel.setText("history of changes");
						
		historyButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
						if (showHistory) {
							showHistory = false;
							linkRoot.toggleHistory(false);
						} else {
							showHistory = true;
							linkRoot.toggleHistory(true);
						}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
			}
		});

		viewer = new TableViewer(c, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);
		Table table = viewer.getTable();

		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		disableSelectionListener = new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			public void widgetSelected(SelectionEvent e) {
				// this seems to be the only way to disable selection display,
				// i.e. the coloured bar that misleads to the assumption it is
				// relevant when the user clicks one of the makepermantent
				// buttons.
				viewer.getTable().deselectAll();
			}
		};
		
		viewer.getTable().addSelectionListener(disableSelectionListener);

		// viewer.getTable().setBackground(new Color(null, 255, 255, 255)); //
		// hide selection?

		TableViewerColumn tcChecked = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn tableColumn = tcChecked.getColumn();
		tableColumn.setAlignment(SWT.CENTER);
		tcChecked.getColumn().setText("Selected");
		tcChecked
				.getColumn()
				.setToolTipText(
						"Select links to delete, approve or mark them as doubtful.\r\nWhen the box is\r\n\tgrey, you do not have the rights to modify these links.");
		tcChecked.getColumn().setWidth(55);
		tcChecked.setEditingSupport(new WBLinkEditingSupport(viewer, 0));
		TableViewerColumn tcAppr = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn tableColumn_1 = tcAppr.getColumn();
		tableColumn_1.setAlignment(SWT.CENTER);
		tcAppr.getColumn().setText("Status");
		tcAppr
				.getColumn()
				.setToolTipText(
						"Status of this Link. Will be:\r\n\t(X)\tRED when it is deleted,\r\n\t(+)\tBLUE when manually added but unsaved yet,\r\n\t(LIGHT GREY)\tfor untouched links:\r\n\t(C)\tomputed when calculated,\r\n\t(B)\took when inferred from printed dictionaries,\r\n\t(M)\tanually when manually added by other means,\r\n\t(E)\tLIGHT GREEN when approved by an Editor,\r\n\t(A)\tDARK GREEN when approved by an Administrator, and\r\n\t(?)\tYELLOW when marked as doubtful");
		tcAppr.getColumn().setWidth(45);
		TableViewerColumn tcDesc = new TableViewerColumn(viewer, SWT.NONE);
		tcDesc.getColumn().setText("Target Lemma");
		tcDesc.getColumn().setWidth(120);
		TableViewerColumn tcStart = new TableViewerColumn(viewer, SWT.NONE);
		tcStart.getColumn().setText("Dictionary");
		tcStart.getColumn().setWidth(60);
		TableViewerColumn tcStat = new TableViewerColumn(viewer, SWT.NONE);
		tcStat.getColumn().setText("Status");
		tcStat.getColumn().setWidth(125);
		TableViewerColumn tcSemRel = new TableViewerColumn(viewer, SWT.NONE);
		tcSemRel.getColumn().setText("Semantic");
		tcSemRel.getColumn().setWidth(90);
		tcSemRel.setEditingSupport(new WBLinkEditingSupport(viewer, 5));
		TableViewerColumn tcEtymRel = new TableViewerColumn(viewer, SWT.NONE);
		tcEtymRel.getColumn().setText("Etymological");
		tcEtymRel.getColumn().setWidth(90);
		tcEtymRel.setEditingSupport(new WBLinkEditingSupport(viewer, 6));
		TableViewerColumn tcEdi = new TableViewerColumn(viewer, SWT.NONE);
		tcEdi.getColumn().setText("Editor");
		tcEdi.getColumn().setWidth(120);
		TableViewerColumn tcCom = new TableViewerColumn(viewer, SWT.NONE);
		tcCom.getColumn().setText("Comment");
		tcCom.getColumn().setWidth(400);
		tcCom.setEditingSupport(new WBLinkEditingSupport(viewer, 8));

		viewer.setContentProvider(new ObservableListContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameTimeSorter());
		// viewer.setComparator(new ViewerComparator(new
		// NameWBshortTimestampComparator()));
		viewer.setInput(linkRoot.getList()); // this will update dynamically,
		// no need for setInput() anymore

		GridData vGD = new GridData();
		vGD.horizontalAlignment = GridData.FILL;
		vGD.grabExcessVerticalSpace = true;
		vGD.grabExcessHorizontalSpace = true;
		// vGD.heightHint = 100;
		// vGD.widthHint = 400;
		vGD.verticalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(vGD);

		bottomBar = new Composite(c, SWT.FILL);
		bottomBar.setLayout(new GridLayout(8, false));
		GridDataFactory.fillDefaults().applyTo(bottomBar);

		cancelButton = new Button(bottomBar, SWT.PUSH);
		cancelButton.setText("Cancel changes");
		cancelButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				targetState = false;
				setLemma(sourceLemma);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		deleteButton = new Button(bottomBar, SWT.PUSH);
		deleteButton.setText("Delete selected links");
		deleteButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				deleteSelectedLinks();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		setButton = new Button(bottomBar, SWT.PUSH);
		setButton.setText("Set/Approve selected links");
		setButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				setSelectedLinks();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		doubtButton = new Button(bottomBar, SWT.PUSH);
		doubtButton.setText("Mark selected links doubtful");
		doubtButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				doubtSelectedLinks();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		
		finishButton = new Button(bottomBar, SWT.PUSH);
		finishButton.setText("Finish");
		finishButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				makeBlank();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		finishLabel = new Label(bottomBar, SWT.None);
		finishLabel.setText("");
		
		finishLabel2 = new Label(bottomBar, SWT.BORDER);
		finishLabel2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		finishLabel2.setText("");
		finishLabel2.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD));
		
		new Label(bottomBar, SWT.NONE);
		
		setButtonsEnabled(enableButtons);
	}

	private void setSelectedLinks() {
		int i = 0;
		for (Iterator iterator = linkRoot.getList().iterator(); iterator
				.hasNext();) {
			WBLink oneLink = (WBLink) iterator.next();
			if (oneLink.isTouched()) {
				i++;

				oneLink.setDeleted(false);

				if (whoIsLoggedIn.equals(WBL_ADMIN)) {
					makePermanent(oneLink, 3, false);
				} else {
					// otherwise will be editor
					makePermanent(oneLink, 2, true);
				}

			}
		}
		postWrite(i);
	}

	private void deleteSelectedLinks() {
		int i = 0;
		for (Iterator iterator = linkRoot.getList().iterator(); iterator
				.hasNext();) {
			WBLink oneLink = (WBLink) iterator.next();
			// make sure oneLink is selected and that we only delete links that
			// are already permanent, i.e. have some timestamp set
			if (oneLink.isTouched() && !oneLink.isDeleted()
					&& !WBLink.UNSAVED_TIMESTAMP.equals(oneLink.getTimestamp())) {
				i++;

				oneLink.setDeleted(true);

				oneLink.setSemanticRelationID(-1);
				oneLink.setEtymRelationID(-1);

				if (whoIsLoggedIn.equals(WBL_ADMIN)) {
					makePermanent(oneLink, 3, false);
				} else {
					// otherwise will be editor
					if (oneLink.getQualityID() >= 5) {
						// special case: editor's vote suffices to delete a
						// computed link once and for all
						makePermanent(oneLink, 2, false);
					} else if (oneLink.getQualityID() >= 1
							&& oneLink.getQualityID() <= 3) {
						// special case: book links will be marked only doubtful
						oneLink.setDeleted(false);
						makePermanent(oneLink, 9, true);
					} else {
						makePermanent(oneLink, 2, true);
					}
				}
			}
		}
		postWrite(i);
	}

	private void doubtSelectedLinks() {
		int i = 0;
		for (Iterator iterator = linkRoot.getList().iterator(); iterator
				.hasNext();) {
			WBLink oneLink = (WBLink) iterator.next();
			if (oneLink.isTouched()) {
				i++;
				// System.out.println("Want to approve... "
				// + oneLink.getTargetLemma().getName());

				// we can assume editors will not be able to doubt
				// admin-approved links as those are immutable for them
				makePermanent(oneLink, 9, true);
			}
		}
		postWrite(i);
	}

	/**
	 * Writes a single link to the database.
	 * 
	 * @param l
	 * @param approve
	 * @param editable
	 */
	private void makePermanent(WBLink l, int approve, boolean editable) {
		boolean success = false;
		String sessionId = RBACSession.getInstance().getSID(true);

		// only the link's comment is free text that can contain evil ä's, &'s,
		// <'s and other bad stuff.
		String encodedComment = "";
		try {
			encodedComment = URLEncoder.encode(l.getComment(), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
		}

		if (DEBUG_OUTPUT) {
			System.out
					.println("Want to permanentify a link, with these variables: "
							+ l.getSourceLemma().getID()
							+ "|"
							+ l.getSourceLemma().getWbShortName()
							+ "|"
							+ l.getTargetLemma().getID()
							+ "|"
							+ l.getTargetLemma().getWbShortName()
							+ "|"
							+ RBACSession.getInstance().getEPPN()
							+ "|"
							+ Integer.toString(l.getQualityID())
							+ "|"
							+ "comment:"
							+ encodedComment
							+ "|"
							+ Integer.toString(l.getSemanticRelationID())
							+ "|"
							+ Integer.toString(l.getEtymRelationID())
							+ "|"
							+ sessionId
							+ "|approve:"
							+ approve
							+ "|deleted:"
							+ (l.isDeleted() ? "1" : "0")
							+ "|editable:"
							+ (editable ? "1" : "0"));
		}
		//TODO JaxRpc things
//		try {
//			String response = stub.wbb_setLink(l.getSourceLemma().getID(), l
//					.getSourceLemma().getWbShortName(), l.getTargetLemma()
//					.getID(), l.getTargetLemma().getWbShortName(), RBACSession
//					.getInstance().getEPPN(), Integer
//					.toString(l.getQualityID()), encodedComment, Integer
//					.toString(l.getSemanticRelationID()), Integer.toString(l
//					.getEtymRelationID()), sessionId,
//					Integer.toString(approve), l.isDeleted() ? "1" : "0",
//					editable ? "1" : "0");
//			if (DEBUG_OUTPUT) {
//				System.out.println("Response of permanentification: "
//						+ response);
//			}
//			if ("false".equals(response)) {
//				failedToWrite++;
//			} else {
//				success = true;
//			}
//		} catch (RemoteException e) {
//			Activator.handleProblem(IStatus.ERROR, e,
//					"Could not write this link to wbb");
//			failedToWrite++;
//		}
		
		//#####################################################################################
		try {
			String response = wbservice.getWbb_WebServiceSoap().wbb_setLink(l.getSourceLemma().getID(), l
						.getSourceLemma().getWbShortName(), l.getTargetLemma()
						.getID(), l.getTargetLemma().getWbShortName(), RBACSession
						.getInstance().getEPPN(), Integer
						.toString(l.getQualityID()), encodedComment, Integer
						.toString(l.getSemanticRelationID()), Integer.toString(l
						.getEtymRelationID()), sessionId,
						Integer.toString(approve), l.isDeleted() ? "1" : "0",
						editable ? "1" : "0");
			
			if (DEBUG_OUTPUT) {
				System.out.println("Response of permanentification: "
						+ response);
			}
			if ("false".equals(response)) {
				failedToWrite++;
			} else {
				success = true;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
			"Could not write this link to wbb");
			failedToWrite++;
		} 
		
		
		//#####################################################################################
		
		if (success) {
			linkRoot.removeFromUnprocessedList(l);
		}
	}

	/**
	 * Takes global int failedToWrite, compares, shows message when difference.
	 * Then re-populates the link list.
	 * 
	 * @param numLinksWantedToWrite
	 */
	private void postWrite(int numLinksWantedToWrite) {
		if (failedToWrite > 0) {
			showMessage("Could not write " + failedToWrite
					+ " links to the database (out of " + numLinksWantedToWrite
					+ ").");
		}
		failedToWrite = 0;
		linkRoot.retrieveLinks();
		linkRoot.toggleHistory(showHistory);
	}

	private void makeBlank() {
		targetState = false;
		sourceLabel.setText("<please select>");
		sourceLabel.setToolTipText("Select <source Lemma (Link)> from the \"Dictionary Search Results\"-View.");
		sourceWBLabel.setText("");
		topHint.setText("");
		topBar.pack();
		finishLabel.setText("");
		finishLabel2.setText("");
		bottomBar.pack();
		sourceLemma = null;
		linkRoot.setSourceLemma(null);
		cancelButton.setEnabled(false);
		deleteButton.setEnabled(false);
		setButton.setEnabled(false);
		doubtButton.setEnabled(false);
		finishButton.setEnabled(false);
		historyButton.setEnabled(false);
		toggleHistoryLabel.setEnabled(false);
		// viewer.setInput(null);
	}

	/**
	 * Set source lemma (default) or add a target lemma to the list depending on
	 * the targetState.
	 * 
	 * @param wbLemma
	 */
	public void setLemma(WBLemma wbLemma) {
		setButtonsEnabledIfOk();
		if (targetState) {
			if (sourceLemma.getID().equals(wbLemma.getID())) {
				showMessage("Cannot make a link that points back to the source lemma.");
				return;
			}
			if (WBLinkGroup.isInRegistry(sourceLemma.getID(), wbLemma.getID())) {
				showMessage("This link is already present, please edit the existing one(s) instead of adding a new one.");
				return;
			}

			WBLink addedLink = linkRoot.addNewLink(wbLemma);
			viewer.reveal(addedLink);

		} else {
			sourceLemma = wbLemma;
			linkRoot.setSourceLemma(wbLemma);
			linkRoot.toggleHistory(showHistory);
			// System.out.println("Source Lemma:" + wbLemma.getName());
			sourceLabel.setText("  "+wbLemma.getName()+"  ");
			sourceWBLabel.setText(" "+wbLemma.getWbShortName()+" ");
			sourceWBLabel.setForeground(wbLemma.getWbFgColor());
			topHint.setText("<add new target lemmas or modify existing ones>");
			topHint
					.setToolTipText("Add target lemmas by clicking on their @ or -- sign, or edit existing ones here");
			topBar.pack();
			finishLabel.setText("editing");
			finishLabel2.setText("  "+wbLemma.getName()+"  ");
			bottomBar.pack();
			targetState = true;
		}
	}

	private void setButtonsEnabledIfOk() {
		sourceLabel.setToolTipText("");
		finishButton.setEnabled(true);
		cancelButton.setEnabled(true);
		if (enableButtons) {
			deleteButton.setEnabled(true);
			setButton.setEnabled(true);
			doubtButton.setEnabled(true);
			historyButton.setEnabled(true);
			toggleHistoryLabel.setEnabled(true);
		}
	}

	/**
	 * setting target state to true means that the view will now operate in
	 * target mode, meaning setLemma will add new Links with the existing source
	 * and the given Lemma as target. If target state is false (default)
	 * setLemma will set the source Lemma and retrieve all Links from the
	 * database.
	 * 
	 * @param wantTargetState
	 */
	public void setTargetState(boolean wantTargetState) {
		targetState = wantTargetState;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				DictionaryLinkEditor.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(action2);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				showMessage("Action 1 executed");
			}
		};
		action1.setText("Action 1");
		action1.setToolTipText("Action 1 tooltip");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		action2 = new Action() {
			public void run() {
				showMessage("Action 2 executed");
			}
		};
		action2.setText("Action 2");
		action2.setToolTipText("Action 2 tooltip");
		action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				"Dictionary Link Editor", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private String findOutWhoIsLoggedIn(String sid) {
		if (!"".equals(sid)) {
			if (TextGridProject.checkForRole(WBL_AUTHZ_PROJECT, WBL_ADMIN)) {
				whoIsLoggedIn = WBL_ADMIN;
				enableButtons = true;
			} else if (TextGridProject.checkForRole(WBL_AUTHZ_PROJECT,
					WBL_EDITOR)) {
				whoIsLoggedIn = WBL_EDITOR;
				enableButtons = true;
			} else {
				whoIsLoggedIn = "";
				enableButtons = false;
			}
		} else {
			whoIsLoggedIn = "";
			enableButtons = false;
		}
		if (DEBUG_OUTPUT) {
			System.out.println("Maximal rights of user: " + whoIsLoggedIn);
		}
		return whoIsLoggedIn;
	}

	private void setButtonsEnabled(boolean enableButtons) {
		if (!finishButton.isEnabled()) {
			setButton.setEnabled(false);
			deleteButton.setEnabled(false);
			doubtButton.setEnabled(false);
			historyButton.setEnabled(false);
			toggleHistoryLabel.setEnabled(false);
		} else {
			setButton.setEnabled(enableButtons);
			deleteButton.setEnabled(enableButtons);
			doubtButton.setEnabled(enableButtons);
			historyButton.setEnabled(enableButtons);
			toggleHistoryLabel.setEnabled(enableButtons);
		}
		bottomBar.pack();
	}

	public synchronized void dispose() {
		if (sidChangedListener != null) {
			AuthBrowser.removeSIDChangedListener(sidChangedListener);
		}
		if (disableSelectionListener != null && !viewer.getTable().isDisposed()) {
			
			viewer.getTable().removeSelectionListener(disableSelectionListener);
		}
		super.dispose();
	}
}