package info.textgrid.lab.dictionarylinkeditor;

import org.eclipse.core.runtime.PlatformObject;

class GroupAttributes extends PlatformObject {
	private boolean editable = true;
	private String lastTimestamp = "";

	GroupAttributes(boolean b) {
		this.editable = b;
	}

	GroupAttributes(String timestamp) {
		this.lastTimestamp = timestamp;
	}

	public void setLastTimestamp(String timestamp) {
		this.lastTimestamp = timestamp;
	}

	public String getLastTimestamp() {
		return lastTimestamp;
	}

	public boolean isEditable() {
		return editable;
	}

}
