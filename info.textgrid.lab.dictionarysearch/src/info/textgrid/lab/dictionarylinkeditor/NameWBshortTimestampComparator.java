package info.textgrid.lab.dictionarylinkeditor;

import java.util.Comparator;
/**
 * unused currently, see inner class in DictionaryLinkEditor.
 * @author martin
 *
 */
public class NameWBshortTimestampComparator implements Comparator<WBLink> {

	public int compare(WBLink wbl1, WBLink wbl2) {
//		System.out.println("Found an object of class "+o1.getClass() + ", value: "+o1.toString());
//		
//		WBLink wbl1 = (WBLink) o1;
//		System.out.println("class now: "+wbl1.getClass());
//		
//		WBLink wbl2 = (WBLink) o2;

		int level1 = wbl1.getTargetLemma().getName().compareTo(
				wbl2.getTargetLemma().getName());
		System.out.println("Compared " + wbl1.getTargetLemma().getName() + " with "
				+ wbl2.getTargetLemma().getName() + ", result: " + level1);
		if (level1 != 0)
			return level1;
		else {
			int level2 = wbl1.getTargetLemma().getWbShortName().compareTo(
					wbl2.getTargetLemma().getWbShortName());
			if (level2 != 0)
				return level2;
			else {
				return wbl1.getTimestamp().compareTo(wbl2.getTimestamp());
			}
		}
	}
}
