package info.textgrid.lab.dictionarylinkeditor;

import info.textgrid.lab.dictionarylinkeditor.views.DictionaryLinkEditor;
import org.eclipse.core.runtime.PlatformObject;

/**
 * A Link from one Lemma X in Dictionary A to Lemma Y in Dictionary B. Might be
 * computed or human-edited. Currently, when sending the database a deletion or
 * update request for link L, another Link L' will be inserted with that
 * information such that all editing actions can be traced.
 * 
 * Sorting routine for Links is here, but the DictionaryLinkEditor view uses its
 * own for some reason.
 * 
 * @author martin
 * 
 */
public class WBLink extends PlatformObject implements Comparable<WBLink> {
	public static final String UNSAVED_TIMESTAMP = "9999";
	private WBLemma sourceLemma;
	private WBLemma targetLemma;
	private String editor = null;
	private String timestamp = null;
	private int qualityID;
	private String quality;
	private int etymRelationID;
	private int semanticRelationID;
	private String comment = "";
	private boolean manuallyAdded = false;
	private boolean touched = false;
	private int approved;
	private boolean editable = false;
	private boolean deleted = false;

	/**
	 * Use this constructor if you want to manually create a new Link given a
	 * Source and Target Lemma. Relation, comment and editor will have to be set
	 * using the respective setters. Timestamp will be set when saving and
	 * quality will be added the CHECKED flag.
	 * 
	 * @param sourceLemma
	 * @param targetLemma
	 */
	public WBLink(WBLemma sourceLemma, WBLemma targetLemma) {
		this.sourceLemma = sourceLemma;
		this.targetLemma = targetLemma;
		this.editor = "";
		this.qualityID = 4;
		this.semanticRelationID = -1;
		this.etymRelationID = -1;
		this.comment = "";
		this.timestamp = UNSAVED_TIMESTAMP;
		this.touched = true;
		this.editable = true;
		this.deleted = false;
		this.approved = 0;
		this.manuallyAdded = true;
	}

	/**
	 * Use this constructor when retrieving and instantiating Links from the
	 * database.
	 * 
	 * @param sourceLemma
	 * @param targetLemma
	 * @param editor
	 * @param timestamp
	 * @param quality
	 * @param semanticRelationID
	 * @param etymRelationID
	 * @param comment
	 */
	public WBLink(WBLemma sourceLemma, WBLemma targetLemma, String editor,
			String timestamp, String quality, String semanticRelationID,
			String etymRelationID, String comment, String approved,
			String deleted, String editable) {
		this.sourceLemma = sourceLemma;
		this.targetLemma = targetLemma;
		this.editor = editor;
		this.timestamp = timestamp;
		this.comment = comment;
		this.quality = quality;
		if (quality.matches("^\\d+$")) {
			this.qualityID = (int) new Integer(quality);
		}
		if (approved.matches("^\\d+$")) {
			this.approved = (int) new Integer(approved);
		}
		if ("1".equals(editable)) {
			this.editable = true;
		} else {
			this.editable = false;
		}
		if ("1".equals(deleted)) {
			this.deleted = true;
		} else {
			this.deleted = false;
		}
		if (semanticRelationID.matches("^\\d+$")) {
			this.semanticRelationID = (int) new Integer(semanticRelationID);
		} else {
			this.semanticRelationID = -1;
		}
		if (etymRelationID.matches("^\\d+$")) {
			this.etymRelationID = (int) new Integer(etymRelationID);
		} else {
			this.etymRelationID = -1;
		}
	}

	public WBLemma getSourceLemma() {
		return sourceLemma;
	}

	public WBLemma getTargetLemma() {
		return targetLemma;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getQualityID() {
		return qualityID;
	}

	public String getQualityStringFromID() {
		String result = "";
		switch (qualityID) {
		case -1:
			result = "noch ungespeichert";
			break;
		case 0:
			result = "Gelöscht";
			break;

		case 1:
			result = "Buch";
			break;

		case 2:
			result = "Symmetrisch";
			break;

		case 3:
			result = "Transitiv";
			break;

		case 4:
			result = "Manuell (eingerichtet)";
			break;
		default:
			result = "Berechnet (" + (100 - qualityID) + ")";
		}

		return result;
	}

	public void setQualityID(int qualityID) {
		this.qualityID = qualityID;
	}

	/**
	 * Call this with touch=true when this link undergoes editing, i.e. is newly
	 * created, has some relation or comment change or is clicked the "selected"
	 * checkbox.
	 * 
	 * @param touch
	 */
	public void touch(boolean touch) {
		touched = touch;
	}

	/**
	 * If this is true, delete or approve operations will have to include this
	 * link in their list of links to be written to the database.
	 * 
	 * @return
	 */
	public boolean isTouched() {
		return touched;
	}

	public int getSemanticRelationID() {
		return semanticRelationID;
	}

	public int getEtymRelationID() {
		return etymRelationID;
	}

	public void setSemanticRelationID(int relationID) {
		this.semanticRelationID = relationID;
		touch(true);
	}

	public void setEtymRelationID(int relationID) {
		this.etymRelationID = relationID;
		touch(true);
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
		touch(true);
	}

	public String getQuality() {
		return quality;
	}

	/**
	 * This depends on forWhom (Admin, editor or "") and approved flag and
	 * editable flag.
	 * 
	 * @param forWhom
	 * @return
	 */
	public boolean immutable(String forWhom) {
		if (forWhom.equals("")) {
			return true;
		} else if (forWhom.equals(DictionaryLinkEditor.WBL_EDITOR)
				&& (approved == 3 || (!editable && !(approved == 1)))) {
			return true;
		} else {
			return false;
		}
	}

	public int getApproved() {
		return this.approved;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public boolean isEditable() {
		return this.editable;
	}

	public boolean isDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isManuallyAdded () {
		return manuallyAdded;
	}
	
	public boolean isLastInHistory() {
		return WBLinkGroup.isLastOne(sourceLemma.getID(), targetLemma.getID(),
				timestamp);
	}

	// this could be used for comparison with a NameSorter that does nothing.
	// public String toString() {
	// return targetLemma.getName() + targetLemma.getWbShortName() + timestamp;
	// }

	// this is currently unused, see inner Class NameTimeSorter in DLE view.
	public int compareTo(WBLink o) {
		int level1 = this.targetLemma.getName().compareTo(
				o.getTargetLemma().getName());
		// System.out.println("Compared " + this.targetLemma.getName() + " with
		// "
		// + o.getTargetLemma().getName() + ", result: " + level1);
		if (level1 != 0)
			return level1;
		else {
			int level2 = this.targetLemma.getWbShortName().compareTo(
					o.getTargetLemma().getWbShortName());
			if (level2 != 0)
				return level2;
			else {
				return this.getTimestamp().compareTo(o.getTimestamp());
			}
		}
	}

}
