package info.textgrid.lab.dictionarysearch;

import java.util.ArrayList;
import java.util.List;

/**
 * class DSExactLemma represents the exact lemma from the search results
 * @author wick
 *
 */

public class DSExactLemma {
	private String name;
	private DSLemma associatedLemma;
	private List<DSDictionaryWords> dictionaries;

	/**
	 * constructor of the DSExactLemma class
	 * @param name
	 * @param associatedLemma
	 */
	public DSExactLemma(String name, DSLemma associatedLemma) {
		this.associatedLemma = associatedLemma;
		this.name = name;
		dictionaries = new ArrayList<DSDictionaryWords>();
	}

	/**
	 * adds a dictionaryWords object to the dictionaries list
	 * @param dictionaryWords
	 * @return 
	 */
	public boolean addDictionaryWoerter(DSDictionaryWords dictionaryWords) {
		return dictionaries.add(dictionaryWords);
	}

	/**
	 * returns the associated Searchlemma
	 * @return
	 */
	public DSLemma getAssociatedLemma() {
		return associatedLemma;
	}

	/**
	 * returns the list of dictionaries in which the DSExactLemma occures
	 * @return
	 */
	public List<DSDictionaryWords> getDictionaries() {
		return dictionaries;
	}

	/**
	 * returns the name of the DSExactLemma
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void printName() {
		System.out.println("Exact lemma: " + name);
	}

	public String toString() {
		return name;
	}
}
