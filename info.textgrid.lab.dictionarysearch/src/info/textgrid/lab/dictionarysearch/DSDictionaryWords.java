package info.textgrid.lab.dictionarysearch;

public class DSDictionaryWords {

	/**
	 * class DictionaryWoerter represents the dictionary in the internal data
	 * hierarchy for the tree viewer
	 * 
	 * @author wick
	 * 
	 */
	private String name;
	private String shortName;
	private String line;
	private String bgcolor;
	private String color;
	private String href;
	private Boolean linking;
	private DSExactLemma associatedExactLemma;
	private boolean blockedForeground;
	private boolean blockedBackground;
	private boolean blockedFont;

	/**
	 * constructor of the DSDictionaryWords class
	 * 
	 * @param name
	 * @param shortName
	 * @param line
	 * @param href
	 * @param bgcolor
	 * @param color
	 * @param linking
	 * @param associatedExactLemma
	 */
	public DSDictionaryWords(String name, String shortName, String line,
			String href, String bgcolor, String color, Boolean linking,
			DSExactLemma associatedExactLemma) {
		this.name = name;
		this.shortName = shortName;
		this.line = line;
		this.bgcolor = bgcolor;
		this.color = color;
		this.href = href;
		this.linking = linking;
		this.associatedExactLemma = associatedExactLemma;
		blockedForeground = false;
		blockedBackground = false;
		blockedFont = false;
	}

	/**
	 * returns the associated DSExactLemma
	 * 
	 * @return
	 */
	public DSExactLemma getAssociatedExactLemma() {
		return associatedExactLemma;
	}

	/**
	 * returns the name of the dictionary
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * returns the boolean value of the linking parameter
	 * 
	 * @return
	 */
	public Boolean getLinking() {
		return linking;
	}

	/**
	 * returns the short name of the dictionary
	 * 
	 * @return
	 */
	public String getShort() {
		return shortName;
	}

	/**
	 * returns the value of the first line from the html-return of the
	 * webservice
	 * 
	 * @return
	 */
	public String getLine() {
		return line;
	}

	/**
	 * returns the URL of the dictionary entry as String
	 * 
	 * @return
	 */
	public String getHref() {
		return href;
	}

	/**
	 * returns the background color for the display of the dictionary in the
	 * tree viewer
	 * 
	 * @return
	 */
	public String getBGColor() {
		return bgcolor;
	}

	/**
	 * returns the foreground color for the display of the dictionary in the
	 * tree viewer
	 * 
	 * @return color
	 */
	public String getColor() {
		return color;
	}

	public void printName() {
		System.out.println("Name :" + name);
	}

	/**
	 * returns "name: line";
	 */
	public String toString() {
		return name + ": " + line;
	}

	/**
	 * returns the boolean value of the parameter blockedFont
	 * 
	 * @return boolean
	 */
	public boolean getBlockedFont() {
		return blockedFont;
	}

	/**
	 * returns the boolean value of the parameter blockedForeground
	 * 
	 * @return boolean
	 */
	public boolean getBlockedForeground() {
		return blockedForeground;
	}

	/**
	 * returns the boolean value of the parameter blockedBackground
	 * 
	 * @return boolean
	 */
	public boolean getBlockedBackground() {
		return blockedBackground;
	}

	/**
	 * sets the blockedFont parameter to true
	 */
	public void setBlockedFont() {
		blockedFont = true;
	}

	/**
	 * sets the blockedForeground parameter to true
	 */
	public void setBlockedForeground() {
		blockedForeground = true;
	}

	/**
	 * sets the blockedBackground parameter to true
	 */
	public void setBlockedBackground() {
		blockedBackground = true;
	}
}
