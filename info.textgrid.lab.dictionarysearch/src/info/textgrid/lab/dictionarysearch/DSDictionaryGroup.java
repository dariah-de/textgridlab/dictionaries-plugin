package info.textgrid.lab.dictionarysearch;

import java.util.ArrayList;
import java.util.List;

public class DSDictionaryGroup {
	private String name;
	private DSLibrary associatedLibrary;
	private List<DSDictionary> dictionaries;

	public DSDictionaryGroup(String name, DSLibrary associatedLibrary) {
		this.associatedLibrary = associatedLibrary;
		this.name = name;
		dictionaries = new ArrayList<DSDictionary>();
	}

	public boolean addDictionary(DSDictionary o) {
		return dictionaries.add(o);
	}

	public DSLibrary getAssociatedLibrary() {
		return associatedLibrary;
	}

	public List<DSDictionary> getDictionaries() {
		return dictionaries;
	}

	public String getName() {
		return name;
	}

	public void printName() {
		System.out.println(name);
	}
}
