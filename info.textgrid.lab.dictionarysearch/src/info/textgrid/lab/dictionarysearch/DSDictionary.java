package info.textgrid.lab.dictionarysearch;


public class DSDictionary {
	private String name;
	private String kurz;
	private String color;
	private String bgcolor;
	private DSDictionaryGroup associatedDictionaryGroup;

	public DSDictionary(String name, String kurz, String color,
			String bgcolor, DSDictionaryGroup associatedDictionaryGroup) {
		this.associatedDictionaryGroup = associatedDictionaryGroup;
		this.color = color;
		this.bgcolor = bgcolor;
		this.name = name;
		this.kurz = kurz;
	}

	public DSDictionaryGroup getAssociatedDictionaryGroup() {
		return associatedDictionaryGroup;
	}

	public String getName() {
		return name;
	}

	public String getKurz() {
		return kurz;
	}

	public String getColor() {
		return color;
	}

	public String getBgColor() {
		return bgcolor;
	}

	public void printName() {
		System.out.println(name);
	}

	public boolean getBlockedBackground() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setBlockedBackground() {
		// TODO Auto-generated method stub

	}

	public boolean getBlockedForeground() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setBlockedForeground() {
		// TODO Auto-generated method stub

	}
}
