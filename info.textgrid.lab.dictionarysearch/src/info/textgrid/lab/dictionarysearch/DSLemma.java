package info.textgrid.lab.dictionarysearch;

import java.util.ArrayList;
import java.util.List;

/**
 * class DSLemma represents the lemma from the search results
 * @author wick
 *
 **/

public class DSLemma {
	private String name;
	private List<DSExactLemma> exactLemmas;

	/**
	 * constructor of the DSLemma class
	 * @param name
	 */
	public DSLemma(String name) {
		this.name = name;
		exactLemmas = new ArrayList<DSExactLemma>();
	}

	/**
	 * adds a DSExactLemma object to the exactLemmas list
	 * @param exactLemma
	 * @return
	 */
	public boolean addExactLemma(DSExactLemma exactLemma) {
		return exactLemmas.add(exactLemma);
	}

	/**
	 * returns the list of exactLemmas
	 * @return
	 */
	public List<DSExactLemma> getExactLemmas() {
		return exactLemmas;
	}

	/**
	 * returns the name of the DSLemma
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void printName() {
		System.out.println("Searchlemma: " + name);
	}

	public String toString() {
		return name;
	}

}
