package info.textgrid.lab.dictionarysearch;

import java.util.ArrayList;
import java.util.List;

public class DSLibrary {
	String name;
	List<DSDictionaryGroup> dictionaryGroups;

	public DSLibrary(String name) {
		this.name = name;
		dictionaryGroups = new ArrayList<DSDictionaryGroup>();
	}

	public boolean addDictionaryGroup(DSDictionaryGroup o) {
		return dictionaryGroups.add(o);
	}

	public List<DSDictionaryGroup> getDictionaryGroups() {
		return dictionaryGroups;
	}

	public String getName() {
		return name;
	}

	public void printName() {
		System.out.println(name);
	}
}
