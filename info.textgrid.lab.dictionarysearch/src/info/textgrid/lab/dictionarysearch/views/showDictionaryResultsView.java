package info.textgrid.lab.dictionarysearch.views;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
/**
 * The showDictionaryResultView handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class showDictionaryResultsView extends AbstractHandler {

	/**
	 * the command has been executed, so extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		// TODO Auto-generated method stub
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			if (activePage == null) 
				MessageDialog
				.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), Messages.showDictionaryResultsView_No_Open_Perspective,
						Messages.showDictionaryResultsView_No_Open_Perspective_2);
			else 
				activePage.showView("info.textgrid.lab.dictionarysearch.views.DictionarySearchView"); //$NON-NLS-1$
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(Messages.showDictionaryResultsView_Could_Not_Open_DRV);
			e.printStackTrace();
		}
		return null;
	}

}
