package info.textgrid.lab.dictionarysearch.views;

import info.textgrid.lab.core.browserfix.TextGridLabBrowser;
import info.textgrid.lab.dictionarysearch.DSDictionaryWords;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

/**
 * This view provides the display of the desired entry of the
 * DictionaryResultView in an internal browser.
 * <p>
 * 
 * @author TextGrid / University Trier / Christoph Wick <wick@uni-trier.de>
 * 
 */

public class DictionaryBrowserView extends ViewPart implements
		ISelectionListener {
	public static String ID = "info.textgrid.lab.dictionarysearch.views.DictionaryBrowserView"; //$NON-NLS-1$

	// protected IWebBrowser browser2;
	protected Browser browser;
	private DSDictionaryWords dictionary;

	// private ProgressBar progressBar;

	@Override
	public void dispose() {
		// cf. the end of #createPartControl(Composite)
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(this);
		getSite().getWorkbenchWindow().getSelectionService()
				.removePostSelectionListener(this);
		super.dispose();
	}

	@Override
	public void createPartControl(Composite parent) {
		/**
		 * initalizes the browser
		 */

		browser = TextGridLabBrowser.createBrowser(parent);

		/*
		 * IWorkbenchBrowserSupport browserSupport =
		 * getSite().getWorkbenchWindow().getWorkbench().getBrowserSupport();
		 * try { browser2 =
		 * browserSupport.createBrowser(IWorkbenchBrowserSupport
		 * .AS_EXTERNAL,"myBrowser", "mein Browser", "mein Tooltip"); } catch
		 * (PartInitException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		// progressBar = new ProgressBar(parent, SWT.NONE);
		//
		// FormLayout layout= new FormLayout();
		// parent.setLayout(layout);
		// FormData data1 = new FormData();
		// data1.left = new FormAttachment(0,5);
		// data1.right = new FormAttachment(100,5);
		// data1.top = new FormAttachment(0,5);
		// data1.bottom = new FormAttachment(90,-5);
		// browser.setLayoutData(data1);
		//		
		// FormData data2 = new FormData();
		// data2.top = new FormAttachment(browser, 5);
		// data2.left = new FormAttachment(0,5);
		// data2.right = new FormAttachment(100,5);
		// progressBar.setLayoutData(data2);

		browser.addProgressListener(new ProgressListener() {

			public void changed(ProgressEvent event) {
				StringBuilder msg = new StringBuilder();
				if (dictionary != null)
					msg.append(dictionary.getAssociatedExactLemma().getName())
							.append(" (").append(dictionary.getName()).append( //$NON-NLS-1$
									"), "); //$NON-NLS-1$

				int percent = 0;
				if (event.total == 0)
					percent = 100;
				else
					percent = 100 * (event.current / event.total);
				msg.append(percent).append('%');
				// progressBar.setSelection(percent);

				DictionaryBrowserView.this
						.setContentDescription(msg.toString());
			}

			public void completed(ProgressEvent event) {
				DictionaryBrowserView.this.showBusy(false);
				StringBuilder msg = new StringBuilder();
				if (dictionary != null)
					msg.append(dictionary.getAssociatedExactLemma().getName())
							.append(" (").append(dictionary.getName()).append( //$NON-NLS-1$
									')');
				// progressBar.setSelection(0);
			}
		});
		getSite().getWorkbenchWindow().getSelectionService()
				.addPostSelectionListener(this);
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(this);

		browser
				.setText(Messages.DictionaryBrowserView_Message_1
						+ Messages.DictionaryBrowserView_Message_2);

		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.dictionarysearch.DescriptionGridView"); //$NON-NLS-1$
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		browser.setFocus();
	}

	public void selectionChanged(IWorkbenchPart part, final ISelection selection) {
		/**
		 * reacts of a selection from ResultView
		 */
		// System.out.println("New selection: " + selection.toString());

		if (selection instanceof IStructuredSelection) {
			Object o = ((IStructuredSelection) selection).getFirstElement();
			if (o instanceof DSDictionaryWords) {
				dictionary = (DSDictionaryWords) o;
				DictionaryBrowserView.this.setContentDescription(dictionary
						.getAssociatedExactLemma().getName()
						+ "(" + dictionary.getName() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
				DictionaryBrowserView.this.showBusy(true);

				final UIJob job = new UIJob("Showing Entry for selected Dictionary-Object") { //$NON-NLS-1$

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						if (monitor.isCanceled())
							return Status.CANCEL_STATUS;
						try {
							browser
									.setUrl(dictionary.getHref()
											+ "#navigation"); //$NON-NLS-1$
						} catch (SWTException e) {
							/*
							 * Bugfix TG-534
							 * https://develop.sub.uni-goettingen.de
							 * /jira/browse/TG-534
							 */
							if (e.getMessage() == "ERROR_WIDGET_DISPOSED") { //$NON-NLS-1$
								System.out.println("AHAAAAAAAAAAAAAAAAAAAAAAA"); //$NON-NLS-1$
								return Status.CANCEL_STATUS;}
						}
						return Status.OK_STATUS;
					}

				};
				/*job.addJobChangeListener(new JobChangeAdapter() {
					public void done(IJobChangeEvent event) {
						if (event.getResult().isOK())
							System.out.println("Job completed successfully");
						else
							System.out
									.println("Job did not complete successfully");
					}
				});*/
				//job.setSystem(true);
				job.schedule(); // start as soon as possible
			}
		}
	}

}