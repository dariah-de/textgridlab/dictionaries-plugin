package info.textgrid.lab.dictionarysearch.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.dictionarysearch.views.messages"; //$NON-NLS-1$

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}

	public static String DictionaryBrowserView_Message_1;
	public static String DictionaryBrowserView_Message_2;
	public static String DictionarySearchView_Column_FirstLine;
	public static String DictionarySearchView_Column_Lemma_Dict;
	public static String DictionarySearchView_Column_Link;
	public static String DictionarySearchView_Next;
	public static String DictionarySearchView_Of;
	public static String DictionarySearchView_Page_0_of_0;
	public static String DictionarySearchView_Page;
	public static String DictionarySearchView_Previous;
	public static String DictionarySearchView_Processing;
	public static String DictionarySearchView_Results;
	public static String DictionarySearchView_Search_Result_Info;
	public static String DictionarySearchView_Search_Result_Info_1;
	public static String DictionarySearchView_Searchlemma;
	public static String DictionarySearchView_Searchlemma_2;
	public static String DictionarySearchView_Service_Not_Available;
	public static String DictionarySearchView_View_Name;
	public static String newSearchHandler__No_Open_Perspective_Message;
	public static String newSearchHandler_Could_Not_Open_DRV;
	public static String newSearchHandler_No_Open_Perspectice;
	public static String showDictionaryBrowserView_Could_Not_Open_DBV;
	public static String showDictionaryResultsView_Could_Not_Open_DRV;
	public static String showDictionaryResultsView_No_Open_Perspective;
	public static String showDictionaryResultsView_No_Open_Perspective_2;
	public static String showDictionarySearchView_Could_Not_Open_DS;
}
