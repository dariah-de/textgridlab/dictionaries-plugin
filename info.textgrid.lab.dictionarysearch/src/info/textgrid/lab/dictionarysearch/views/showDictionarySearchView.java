package info.textgrid.lab.dictionarysearch.views;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.PlatformUI;
/**
 * The showDictionaryResultView handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 * @author wick
 */
public class showDictionarySearchView extends AbstractHandler {

	/**
	 * the command has been executed, so extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		// TODO Auto-generated method stub
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
			.showView("info.textgrid.lab.woerterbuchsearchmask.view.DictionarySearchMaskView"); //$NON-NLS-1$
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(Messages.showDictionarySearchView_Could_Not_Open_DS);
			e.printStackTrace();
		}
		return null;
	}

}
