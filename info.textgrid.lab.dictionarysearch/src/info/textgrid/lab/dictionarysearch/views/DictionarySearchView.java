package info.textgrid.lab.dictionarysearch.views;

import info.textgrid.lab.dictionarylinkeditor.WBLemma;
import info.textgrid.lab.dictionarylinkeditor.views.DictionaryLinkEditor;
import info.textgrid.lab.dictionarysearch.Activator;
import info.textgrid.lab.dictionarysearch.DSDictionaryWords;
import info.textgrid.lab.dictionarysearch.DSExactLemma;
import info.textgrid.lab.dictionarysearch.DSLemma;
import info.textgrid.lab.dictionarysearch.client.Wbb_WebService_Impl;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub.Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine;
//import info.textgrid.lab.dictionarysearch.stubs.Wbb_WebServiceStub.Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_Ext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

//import org.apache.axis2.client.Options;
//import org.apache.axis2.client.ServiceClient;
//import org.apache.axis2.transport.http.HTTPConstants;
//import org.apache.axis2.transport.http.HttpTransportProperties.ProxyProperties;
import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.texteditor.ITextEditor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Dictionary Search Results View. This view shows the results of the dictionary
 * search in multiple dictionaries in a tree view. These results can be chosen,
 * so that the entry of the specific dictionary is shown in the Dictionary
 * Browser View.
 * 
 * @author TextGrid / University Trier / Christoph Wick <wick@uni-trier.de>
 * 
 */

public class DictionarySearchView extends ViewPart/*
												 * implements ISelectionListener
												 */{

	private static final String TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER = "http://tg-woerterbuch-webservice.uni-trier.de/Wbb_WebService/"; //$NON-NLS-1$
	// private static final String TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER =
	// "http://urts173.uni-trier.de:8115/Wbb_WebService/";
	private static final int READ_TIME_OUT = 8 * 1000;

	private static final String PROCESSING = Messages.DictionarySearchView_Processing;

	private UIJob job;

	public static String ID = "info.textgrid.lab.dictionarysearch.views.DictionarySearchView"; //$NON-NLS-1$
	public static String search_Lemma = ""; //$NON-NLS-1$
	public static String search_Wbblist = "all"; //$NON-NLS-1$
	public static String search_Limit = "10"; //$NON-NLS-1$
	public static String search_Offset = "0"; //$NON-NLS-1$
	public static String search_Suchart = "u"; //$NON-NLS-1$
	public static String SEARCH_LINKEDLEMMAS = "false"; //$NON-NLS-1$
	public static Boolean SEARCH_FROM_OUTSIDE = false;

	public static Boolean DEBUG_OUTPUT = false; // true to enable some debug
	// output

	private int resultsCount;

	private TreeViewer viewer;
	private DrillDownAdapter drillDownAdapter;
	// private String response;
	private Label labelLemma_pre;
	private Label labelLemma;
	private Label labelResult;
	private Label labelPage;
	private Button buttonPrevious;
	private Button buttonNext;
	private MyISelectionListener mySelectionListener;
	private boolean emptyResult;
	private String thisSearchLemma;
	private int pageCount = 1;
	private int pageNo = 1;
	private boolean wantLinkEditorInsteadOfDictBrowser = false;

	private List<DSLemma> rootElement;
	// private EnterDictionariesUIJob uiJob;

	private DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();

	// Include the Wörterbuch-LinkEditor
	private boolean withWBLE = false;

	private Label labelSearchStatus;

	public TreeViewer getTreeViewer() {
		return viewer;
	}

	/**
	 * 
	 * @author wick
	 * 
	 */
	class MyISelectionListener implements ISelectionListener {
		private final class ShowResultsJob extends UIJob {
			private final ITextSelection textSelection;
			private final IWorkbenchPart part;

			private ShowResultsJob(String name, IWorkbenchPart part,
					ITextSelection textSelection) {
				super(name);
				this.textSelection = textSelection;
				this.part = part;

			}

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				String word = extractCurrentWord(part, textSelection);
				if (word != null && word != "") { //$NON-NLS-1$
					if (DEBUG_OUTPUT)
						showMessage("setInput wird mit " + "<" + word //$NON-NLS-1$ //$NON-NLS-2$
								+ "> aufgerufen!"); //$NON-NLS-1$
					DSLemma newLemma = new DSLemma(word);
					setSearchFromOutside(false);
					viewer.setInput(newLemma);
					viewer.expandAll();
					if (emptyResult) {
						labelLemma.setForeground(new Color(null, 255, 0, 0));
						labelLemma
								.setText(Messages.DictionarySearchView_Search_Result_Info_1
										+ rootElement.get(0).getName()
										+ Messages.DictionarySearchView_Search_Result_Info);
						labelResult.setText(resultsCount
								+ Messages.DictionarySearchView_Results);
						labelPage
								.setText(Messages.DictionarySearchView_Page_0_of_0);
						emptyResult = false;
					} else {
						labelLemma.setForeground(new Color(null, 0, 0, 255));
						labelLemma.setText(word);
						labelResult.setText(resultsCount
								+ Messages.DictionarySearchView_Results);
						pageNo = 1;
						search_Offset = "0"; //$NON-NLS-1$
					}
				}
				return Status.OK_STATUS;
			}

			private String extractCurrentWord(IWorkbenchPart part2,
					ITextSelection selection) {
				if (selection.getLength() > 0) {
					return selection.getText();
				}

				if (part2 instanceof MultiPageEditorPart) {
					MultiPageEditorPart multipage = (MultiPageEditorPart) part2;

					Object adapter = multipage.getAdapter(ITextEditor.class);
					if (adapter != null)
						part2 = (ITextEditor) adapter;
				}

				if (part2 instanceof ITextEditor) {
					ITextEditor editor = (ITextEditor) part2;

					int left;
					int right;
					left = right = selection.getOffset();
					IDocument document = editor.getDocumentProvider()
							.getDocument(selection); // XXX ???
					if (document != null)
						try {
							while (right < document.getLength()
									&& Character.isLetter(document
											.getChar(right)))
								right++;
							while (left >= 0
									&& Character.isLetter(document
											.getChar(left)))
								left--;
							return document.get(left, right - left);

						} catch (BadLocationException e) {
							Activator.handleProblem(IStatus.ERROR, e,
									"Extracting excerpt caused problem"); //$NON-NLS-1$
						}
				}
				return null;
			}
		}

		public void selectionChanged(IWorkbenchPart part,
				final ISelection selection) {
			// if (selection instanceof ITextSelection) {
			// ITextSelection textSelection = (ITextSelection) selection;
			//
			// job = new ShowResultsJob("Showing Results in Dictionaries for "
			// + textSelection.getText(), part, textSelection);
			// job.schedule();
			// } else if (selection instanceof IStructuredSelection) {
			// IStructuredSelection ss = (IStructuredSelection) selection;
			// if (ss.getFirstElement() instanceof DSDictionaryWords) {
			// }
			// }
		}
	}

	/**
	 * The Content Provider is responsible for the content of the treeviewer. It
	 * builds the tree structure and brings the viewer to sight.
	 **/
	class ViewContentProvider implements IStructuredContentProvider,
			ITreeContentProvider {

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			if (oldInput != null && oldInput.equals(newInput))
				return;

			viewer = (TreeViewer) v;

			if (!(newInput instanceof DSLemma)) {
				if (DEBUG_OUTPUT)
					showMessage("neuer Input KEIN lemma, sondern: " //$NON-NLS-1$
							+ newInput.getClass().getSimpleName());
				return;
			}

			if (DEBUG_OUTPUT)
				showMessage("neuer Input: " + newInput.toString() //$NON-NLS-1$
						+ "\n Parameter: \n" + "WBBLIST: " + search_Wbblist //$NON-NLS-1$ //$NON-NLS-2$
						+ "\n" + "LIMIT: " + search_Limit + "\n" + "OFFSET: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						+ search_Offset + "\n" + "SUCHART: " + search_Suchart //$NON-NLS-1$ //$NON-NLS-2$
						+ "\n" + "LINKED: " + SEARCH_LINKEDLEMMAS); //$NON-NLS-1$ //$NON-NLS-2$

			// if the search is not from DictionarySearchMaskView
			if (!SEARCH_FROM_OUTSIDE) {
				search_Suchart = "u"; //$NON-NLS-1$
				// search_Limit="10";
				search_Wbblist = "all"; //$NON-NLS-1$
			}

			// ############################################
			class XMLHelper {

				/**
				 * Returns the string where all non-ascii and <, &, > are
				 * encoded as numeric entities. I.e. "&lt;A &amp; B &gt;" ....
				 * (insert result here). The result is safe to include anywhere
				 * in a text field in an XML-string. If there was no characters
				 * to protect, the original string is returned.
				 * 
				 * @param originalUnprotectedString
				 *            original string which may contain characters
				 *            either reserved in XML or with different
				 *            representation in different encodings (like 8859-1
				 *            and UFT-8)
				 * @return
				 */
				public String protectSpecialCharacters(
						String originalUnprotectedString) {
					if (originalUnprotectedString == null) {
						return null;
					}
					boolean anyCharactersProtected = false;

					StringBuffer stringBuffer = new StringBuffer();
					for (int i = 0; i < originalUnprotectedString.length(); i++) {
						char ch = originalUnprotectedString.charAt(i);

						boolean controlCharacter = ch < 32;
						// boolean unicodeButNotAscii = ch > 126;
						boolean characterWithSpecialMeaningInXML = ch == '<'
								|| ch == '&' || ch == '>';

						if (characterWithSpecialMeaningInXML /*
															 * ||
															 * unicodeButNotAscii
															 */
								|| controlCharacter) {
							stringBuffer.append("&#" + (int) ch + ";"); //$NON-NLS-1$ //$NON-NLS-2$
							anyCharactersProtected = true;
						} else {
							stringBuffer.append(ch);
						}
					}
					if (anyCharactersProtected == false) {
						return originalUnprotectedString;
					}

					return stringBuffer.toString();
				}

			}

			// ############################################
			XMLHelper xhelper = new XMLHelper();

			rootElement = new ArrayList<DSLemma>();

			DSLemma searchLemma = (DSLemma) newInput;
			search_Lemma = searchLemma.getName();

			setThisSearchLemma(searchLemma.getName());
			rootElement.add(searchLemma);
			String responseString = new String();
			List<DSDictionaryWords> resultList = new ArrayList<DSDictionaryWords>();

			try {
				responseString = getMatchingDictionaries(xhelper
						.protectSpecialCharacters(rootElement.get(0).getName()));
				resultList = response2List(responseString);
			} catch (SAXException saxE) {
				if (DEBUG_OUTPUT)
					showMessage("FEHLER beim Parsen!" + "\n saxE.getMessage()"); //$NON-NLS-1$ //$NON-NLS-2$
			} catch (ParserConfigurationException e) {
				System.out.println("ParserConfigurationException!!!! " //$NON-NLS-1$
						+ e.getMessage());
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"ParseConfigurationException", e); //$NON-NLS-1$
				Activator.getDefault().getLog().log(status);
			} catch (IOException e) {
				System.out.println("IOException!!!! " + e.getMessage()); //$NON-NLS-1$
				System.out.println(responseString);
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"IOException", e); //$NON-NLS-1$
				if (e.getMessage().equals("Connection refused: connect")) //$NON-NLS-1$
					showMessage(Messages.DictionarySearchView_Service_Not_Available);
				Activator.getDefault().getLog().log(status);
			}
			DSExactLemma tempLemma = null;
			for (int i = 0; i < resultList.size(); i++) {

				if (tempLemma == null
						|| tempLemma.getName() != resultList.get(i)
								.getAssociatedExactLemma().getName()) {
					// System.out.println(i);
					tempLemma = resultList.get(i).getAssociatedExactLemma();
					rootElement.get(0).addExactLemma(tempLemma);
					DSDictionaryWords tempDict = resultList.get(i);
					tempLemma.addDictionaryWoerter(tempDict);
				} else if (tempLemma.getName() == resultList.get(i)
						.getAssociatedExactLemma().getName()) {
					DSDictionaryWords tempDict = resultList.get(i);
					tempLemma.addDictionaryWoerter(tempDict);
				}
			}
			if (emptyResult) {
				buttonPrevious.setEnabled(false);
				buttonNext.setEnabled(false);
			} else {
				buttonPrevious.setEnabled(true);
				buttonNext.setEnabled(true);
			}
			if (pageNo == pageCount) {
				buttonNext.setEnabled(false);
			}
			if (pageNo == 1) {
				buttonPrevious.setEnabled(false);
			}
		}

		public void dispose() {
			if (getSite().getSelectionProvider() != null)
				getSite().setSelectionProvider(null);
			if (mySelectionListener != null)
				getSite().getWorkbenchWindow().getSelectionService()
						.removePostSelectionListener(mySelectionListener);
			if (job != null && job.getState() == Job.RUNNING)
				job.cancel();
		}

		public Object[] getElements(Object parent) {
			if (parent.equals(getViewSite())) {
				if (rootElement == null)
					initialize();
				return getChildren(rootElement);
			} else if (parent instanceof DSLemma) {
				return getChildren(parent);
			}
			return getChildren(parent);
		}

		public Object getParent(Object child) {
			if (child instanceof DSExactLemma) {
				return ((DSExactLemma) child).getAssociatedLemma();
			} else if (child instanceof DSDictionaryWords) {
				return ((DSDictionaryWords) child).getAssociatedExactLemma();
			}
			return null;
		}

		@SuppressWarnings("unchecked")
		public Object[] getChildren(Object parent) {
			if (parent instanceof List<?>) {
				// //showMessage("List: " +
				// ((List<DictionaryWoerter>)parent).toArray().toString());
				return ((List<DSDictionaryWords>) parent).toArray();
			} else if (parent instanceof DSLemma) {
				// //showMessage("DSLemma: " +
				// ((DSLemma)parent).getExactLemmas().toArray().toString());
				return ((DSLemma) parent).getExactLemmas().toArray();
			} else if (parent instanceof DSExactLemma) {
				// //showMessage("DSExactLemma: " +
				// ((DSExactLemma)parent).getDictionaries().toArray().toString());
				return ((DSExactLemma) parent).getDictionaries().toArray();
			}
			return new Object[0];
		}

		public boolean hasChildren(Object parent) {
			if (parent instanceof List<?>) {
				return ((List<?>) parent).size() > 0;
			} else if (parent instanceof DSLemma) {
				return ((DSLemma) parent).getExactLemmas().size() > 0;
			} else if (parent instanceof DSExactLemma) {
				return ((DSExactLemma) parent).getDictionaries().size() > 0;
			}
			return false;
		}

		/*
		 * We will set up a dummy model to initialize tree hierarchy. In a real
		 * code, you will connect to a real model and expose its hierarchy.
		 */
		private void initialize() {
		}
	}

	/**
	 * This label provider provides the textual content of the tree viewer. It
	 * gets its information from the data hierarchy represented by the classes
	 * DSLemma, DSExactLemma and DSDictionaryWords.
	 * 
	 * @author wick
	 * 
	 */
	class ViewLabelProvider extends LabelProvider implements IFontProvider,
			IColorProvider, ITableLabelProvider {

		@Override
		public String getText(Object obj) {
			/*
			 * if (obj instanceof DSLemma) return ((DSLemma)obj).getName(); else
			 * if (obj instanceof DSExactLemma) return
			 * ((DSExactLemma)obj).getName(); else if (obj instanceof
			 * DictionaryWoerter) return (((DictionaryWoerter)obj).getName() +
			 * ", " + ((DictionaryWoerter)obj).getZeile()); return
			 * obj.toString();
			 */
			return null;
		}

		@Override
		public Image getImage(Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof DSLemma)
				imageKey = ISharedImages.IMG_OBJ_FOLDER;
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(imageKey);
		}

		public Color getBackground(Object element) {
			// TODO Auto-generated method stub

			if (element instanceof DSDictionaryWords) {
				DSDictionaryWords dict = (DSDictionaryWords) element;
				if (!dict.getBlockedBackground()) {
					String stringColor = dict.getBGColor();
					String substring1 = stringColor.substring(1, 3);
					String substring2 = stringColor.substring(3, 5);
					String substring3 = stringColor.substring(5, 7);
					int redValue = Integer.parseInt(substring1, 16);
					int greenValue = Integer.parseInt(substring2, 16);
					int blueValue = Integer.parseInt(substring3, 16);

					Color color = new Color(null, redValue, greenValue,
							blueValue);
					dict.setBlockedBackground();
					return color;
				} else {
					Color color = new Color(null, 238, 238, 238);
					return color;
				}

			}
			// Color color = new Color(null, 238, 238, 238);
			return null;
		}

		public Color getForeground(Object element) {
			// TODO Auto-generated method stub
			if (element instanceof DSDictionaryWords) {
				DSDictionaryWords dict = (DSDictionaryWords) element;
				if (!dict.getBlockedForeground()) {
					String stringColor = dict.getColor();
					String substring1 = stringColor.substring(1, 3);
					String substring2 = stringColor.substring(3, 5);
					String substring3 = stringColor.substring(5, 7);
					int redValue = Integer.parseInt(substring1, 16);
					int greenValue = Integer.parseInt(substring2, 16);
					int blueValue = Integer.parseInt(substring3, 16);

					Color color = new Color(null, redValue, greenValue,
							blueValue);
					dict.setBlockedForeground();
					return color;
				} else {
					Color color = new Color(null, 0, 0, 0);
					return color;
				}

			}
			return null;
		}

		public Font getFont(Object element) {
			if (element instanceof DSDictionaryWords) {
				DSDictionaryWords dict = (DSDictionaryWords) element;
				if (!dict.getBlockedFont()) {
					Font returnValue = null;
					Font defaultFont = JFaceResources.getDefaultFont();
					FontData[] data = defaultFont.getFontData();
					for (int i = 0; i < data.length; i++) {
						data[i].setStyle(SWT.BOLD);
					}
					returnValue = new Font(Display.getDefault(), data);
					dict.setBlockedFont();
					return returnValue;
				} else {
					Font returnValue = null;
					Font defaultFont = JFaceResources.getDefaultFont();
					FontData[] data = defaultFont.getFontData();
					for (int i = 0; i < data.length; i++) {
						data[i].setStyle(SWT.NORMAL);
					}
					returnValue = new Font(Display.getDefault(), data);
					dict.setBlockedFont();
					return returnValue;
				}
			}
			return null;
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		public String getColumnText(Object element, int columnIndex) {
			switch (columnIndex) {
			case 0:
				if (element instanceof DSLemma)
					return ((DSLemma) element).getName();
				else if (element instanceof DSExactLemma)
					return ((DSExactLemma) element).getName();
				else if (element instanceof DSDictionaryWords)
					return ((DSDictionaryWords) element).getShort();
				// not needed in Revision 1.0
			case 1:
				if (withWBLE) {
					if (element instanceof DSDictionaryWords)
						if (((DSDictionaryWords) element).getLinking())
							return "@"; //$NON-NLS-1$
						else
							return "--"; //$NON-NLS-1$
				} else {
					if (element instanceof DSDictionaryWords)
						return ((DSDictionaryWords) element).getLine();
				}
			case 2:
				if (withWBLE) {
					if (element instanceof DSDictionaryWords)
						return ((DSDictionaryWords) element).getLine();
				}

			}
			return null;
		}
	}

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public DictionarySearchView() {
	}

	/**
	 * starts the search in the "Trierer W�rterbuchnetz" webservice.
	 * 
	 * @param lemma
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	protected String getMatchingDictionaries(String lemma)
			throws ParserConfigurationException, SAXException, IOException {

		/*
		 * Change-Parameter for testing the webservices; true =
		 * Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_ext; false =
		 * Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine
		 */
		final boolean WEBSERVICE = true;

		// TODO JaxRpc things

		// Wbb_WebServiceStub wbservice = null;
		// if (WEBSERVICE) {
		// wbservice = new Wbb_WebServiceStub();
		// @SuppressWarnings("unused")
		// Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_Ext service = new
		// Wbb_WebServiceStub.Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_Ext();
		// } else {
		// wbservice = new Wbb_WebServiceStub();
		// Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine service = new
		// Wbb_WebServiceStub.Wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine();
		// }

		// service.setLemma(lemma);
		// service.setWbbList(search_Wbblist);
		// service.setLimit(search_Limit);
		// service.setOffset(search_Offset);
		// service.setSuchArt(search_Suchart);
		// service.setLinking(SEARCH_LINKEDLEMMAS);

		// service.setInput("Insel#all#u#0#10#true");

		// ###################################################################################
		// TODO...

		// ###################################################################################

		// TODO JaxRpc things
		// wbservice._getServiceClient().getOptions().setProperty(
		// HTTPConstants.CHUNKED, false);
		//
		// //Proxy Settings
		// if (info.textgrid.lab.conf.ConfPlugin.getDefault()
		//				.getPreferenceStore().getBoolean("proxy_connection")) { //$NON-NLS-1$
		// ServiceClient serviceClient = wbservice._getServiceClient();
		// Options options = serviceClient.getOptions();
		// ProxyProperties proxyProperties = new ProxyProperties();
		// proxyProperties.setProxyName(info.textgrid.lab.conf.ConfPlugin
		// .getDefault().getPreferenceStore()
		//					.getString("proxy_connection_http")); //$NON-NLS-1$
		// proxyProperties.setProxyPort(info.textgrid.lab.conf.ConfPlugin
		// .getDefault().getPreferenceStore()
		//					.getInt("proxy_connection_port")); //$NON-NLS-1$
		// options.setProperty(
		// org.apache.axis2.transport.http.HTTPConstants.PROXY,
		// proxyProperties);
		// options.setProperty(
		// org.apache.axis2.transport.http.HTTPConstants.HTTP_PROTOCOL_VERSION,
		// org.apache.axis2.transport.http.HTTPConstants.HEADER_PROTOCOL_10);
		// }
		//
		//
		// String response;
		// if (WEBSERVICE) {
		// response = wbservice.wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_Ext(
		// lemma, search_Wbblist, search_Suchart, search_Offset,
		// search_Limit, SEARCH_LINKEDLEMMAS);
		// } else {
		// response = wbservice
		// .wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine(lemma,
		// search_Wbblist, search_Suchart, search_Offset,
		// search_Limit);
		// }
		// if (DEBUG_OUTPUT) {
		//			showMessage("request: " //$NON-NLS-1$
		// + wbservice._getServiceClient().getAxisService()
		//							.getOperationBySOAPAction("Request")); //$NON-NLS-1$
		//			showMessage("Antwort: " + "\n" + response); //$NON-NLS-1$ //$NON-NLS-2$
		// System.out.print(response);
		// }

		// ###################################################################################
		// TODO
		// Now the webservice is calling without Axis
		String response = "";
		try {
			Wbb_WebService_Impl wbservice = new Wbb_WebService_Impl();
			response = wbservice.getWbb_WebServiceSoap()
					.wbb_LemmaSearch4_NEW_RCP2Xml_FirstLine_Ext(lemma,
							search_Wbblist, search_Suchart, search_Offset,
							search_Limit, SEARCH_LINKEDLEMMAS);

			if (DEBUG_OUTPUT) {
				showMessage("Antwort: " + "\n" + response); //$NON-NLS-1$ //$NON-NLS-2$
				System.out.print(response);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Activator.handleProblem(IStatus.ERROR, e,
					Messages.DictionarySearchView_Service_Not_Available
							+ "/nErrorMessage: " + e.getMessage(), "");
			e.printStackTrace();
		}

		// ###################################################################################

		// this.response = response.get_return();
		SEARCH_LINKEDLEMMAS = "false"; //$NON-NLS-1$
		// search_Limit = "10";
		// search_Offset = "0";
		if (!SEARCH_FROM_OUTSIDE) {
			search_Suchart = "u"; //$NON-NLS-1$
			search_Limit = "10"; //$NON-NLS-1$
			// search_Offset="0";
			search_Wbblist = "all"; //$NON-NLS-1$
		}
		// TODO JaxRpc things
		return response;
	}

	/**
	 * parses the XML response of the dictionary webservice in a list object.
	 * 
	 * @param response
	 * @return List<DictionaryWoerter>
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	protected List<DSDictionaryWords> response2List(final String response)
			throws IOException, ParserConfigurationException, SAXException {
		final String responseText;
		responseText = response;

		// System.out.println(response);
		ErrorHandler handler = new ErrorHandler() {
			public void warning(SAXParseException e) throws SAXException {
				System.err.println("[warning] " + e.getMessage()); //$NON-NLS-1$
			}

			public void error(SAXParseException e) throws SAXException {
				System.err.println("[error] " + e.getMessage()); //$NON-NLS-1$
			}

			public void fatalError(SAXParseException e) throws SAXException {
				System.err.println("[fatal error] " + e.getMessage() + " || " //$NON-NLS-1$ //$NON-NLS-2$
						+ "Column: " + e.getColumnNumber() + "; Line: " //$NON-NLS-1$ //$NON-NLS-2$
						+ e.getLineNumber());
				Date dt = new Date();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss.S"); //$NON-NLS-1$
				long time = System.currentTimeMillis();
				File file = new File("c:/textgrid_errors/" + time + ".txt"); //$NON-NLS-1$ //$NON-NLS-2$
				File xmlFile = new File("c:/textgrid_errors/" + time + ".xml"); //$NON-NLS-1$ //$NON-NLS-2$
				BufferedWriter bw;
				BufferedWriter bwXML;
				try {
					bw = new BufferedWriter(new FileWriter(file, true));
					bwXML = new BufferedWriter(new FileWriter(xmlFile, true));
					bw.write("Date = " + df.format(dt)); //$NON-NLS-1$
					bw.write("XML-Filename: " + xmlFile.getName()); //$NON-NLS-1$
					bw.newLine();
					bw.write("[Fatal Error] " + e.getMessage() + " || " //$NON-NLS-1$ //$NON-NLS-2$
							+ "Column: " + e.getColumnNumber() + "; Line: " //$NON-NLS-1$ //$NON-NLS-2$
							+ e.getLineNumber());
					bw.flush();
					bw.close();

					bwXML.write(response);
					bwXML.flush();
					bwXML.close();
				} catch (IOException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID, "IOException", e); //$NON-NLS-1$
					Activator.getDefault().getLog().log(status);
				}
			}
		};

		DocumentBuilder parser = factory.newDocumentBuilder();
		parser.setErrorHandler(handler);

		Document document;

		// System.out.println(response);
		document = parser
				.parse(new InputSource(new StringReader(responseText)));

		NodeList woerterbuchNodeList = document.getElementsByTagName("list"); //$NON-NLS-1$
		NodeList kurzNodeList = document.getElementsByTagName("kurz"); //$NON-NLS-1$
		NodeList wurzelElement = document.getElementsByTagName("resultlist"); //$NON-NLS-1$

		Element rElement = (Element) wurzelElement.item(0);
		DSLemma sLemma = new DSLemma(rElement.getAttribute("searchlemma")); //$NON-NLS-1$
		resultsCount = Integer.valueOf(rElement.getAttribute("n")).intValue(); //$NON-NLS-1$
		// setting of resulting page numbers
		if (resultsCount > 0) {
			if (resultsCount % Integer.parseInt(search_Limit) == 0)
				pageCount = resultsCount / Integer.parseInt(search_Limit);
			else
				pageCount = resultsCount / Integer.parseInt(search_Limit) + 1;
			labelPage.setText(Messages.DictionarySearchView_Page + pageNo
					+ Messages.DictionarySearchView_Of + pageCount);
		}
		if (rElement.getAttribute("n").equals("0")) { //$NON-NLS-1$ //$NON-NLS-2$
			emptyResult = true;
			// label2.setText("Die Stichwortsuche nach" + sLemma.getName() + "
			// ergab keinen Treffer");
		} else
			emptyResult = false;
		// this.rootElement.add(sLemma);

		// Element wb;
		final List<DSDictionaryWords> dictionaries = new ArrayList<DSDictionaryWords>();

		int anzahl = woerterbuchNodeList.getLength();
		for (int i = 0; i < anzahl; i++) {
			Element woerterbuch = (Element) woerterbuchNodeList.item(i);
			// System.out.println("WB: "+ woerterbuch);
			Element knoten = (Element) kurzNodeList.item(i);

			String lemma = woerterbuch.getParentNode().getAttributes()
					.getNamedItem("lemma").getTextContent(); //$NON-NLS-1$
			String name = woerterbuch.getElementsByTagName("name").item(0) //$NON-NLS-1$
					.getFirstChild().getNodeValue();
			// System.out.println("Name: " + name);
			String kurz = woerterbuch.getElementsByTagName("kurz").item(0) //$NON-NLS-1$
					.getFirstChild().getNodeValue();
			// System.out.println("Kurz: " + kurz);

			String bgcolor = knoten.getAttribute("bgcolor"); //$NON-NLS-1$
			// System.out.println("BGColor: " + bgcolor);
			String color = knoten.getAttribute("color"); //$NON-NLS-1$
			// System.out.println("Color: " + color);

			String url = woerterbuch.getElementsByTagName("href").item(0) //$NON-NLS-1$
					.getFirstChild().getNodeValue();
			// System.out.println("URL: " + url);

			// Check if the "zeile"-tag in the answer from the webservice is
			// empty, if so
			// woerterbuch.getElementsByTagName("zeile").item(0).getFirstChild()
			// will throw
			// a NullPointerException
			String zeile = ""; //$NON-NLS-1$
			if ((woerterbuch.getElementsByTagName("zeile").item(0).getFirstChild()) == null) //$NON-NLS-1$
				zeile = ""; //$NON-NLS-1$
			else
				zeile = woerterbuch
						.getElementsByTagName("zeile").item(0).getFirstChild().getNodeValue(); //$NON-NLS-1$
			/*
			 * try { zeile = woerterbuch.getElementsByTagName("zeile").item(0)
			 * .getFirstChild().getNodeValue(); } catch (NullPointerException
			 * npe) { zeile ="";
			 * System.out.println("NullPointerException abgefangen"); }
			 */
			// System.out.println("Zeile: " + zeile);

			String templinking = woerterbuch.getElementsByTagName("linking") //$NON-NLS-1$
					.item(0).getFirstChild().getNodeValue();
			Boolean linking = false;
			if (templinking.equals("true")) //$NON-NLS-1$
				linking = true;

			/*
			 * Empty answers and e.g. "XXX"-entries are written in a logfile for
			 * debugging-purposes
			 */
			if (name.equals("XXX")) { //$NON-NLS-1$
				Date dt = new Date();
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss.S"); //$NON-NLS-1$
				File file = new File("c:/wrongentries.txt"); //$NON-NLS-1$
				BufferedWriter bw = new BufferedWriter(new FileWriter(file,
						true));

				bw.write("Date = " + df.format(dt) + " | "); //$NON-NLS-1$ //$NON-NLS-2$
				bw.write("DSLemma: " + lemma + " # " + "Kurz: " + kurz + " # " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						+ "URL: " + url); //$NON-NLS-1$
				bw.newLine();
				bw.flush();
				bw.close();
				continue;
			}

			DSDictionaryWords tempdict = new DSDictionaryWords(name, kurz,
					zeile, url, bgcolor, color, linking, new DSExactLemma(
							woerterbuch.getParentNode().getAttributes().item(1)
									.getTextContent(), sLemma));
			dictionaries.add(tempdict);
		}

		return dictionaries;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		emptyResult = false;

		if (!checkWebService()) {
			MessageDialog.openError(Display.getCurrent().getActiveShell(),
					"Service Error", //$NON-NLS-1$
					Messages.DictionarySearchView_Service_Not_Available);

			closePerspective();
		}
		GridLayout gridLayout = new GridLayout(4, false);
		parent.setLayout(gridLayout);

		labelLemma_pre = new Label(parent, SWT.NONE);
		labelLemma_pre.setForeground(new Color(null, 0, 0, 0));
		labelLemma_pre.setText(Messages.DictionarySearchView_Searchlemma);

		labelLemma = new Label(parent, SWT.WRAP);
		labelLemma.setForeground(new Color(null, 0, 0, 255));
		GridData gridData = new GridData();
		gridData = new GridData(140, SWT.DEFAULT);
		labelLemma.setLayoutData(gridData);
		labelLemma.setText(Messages.DictionarySearchView_Searchlemma_2);

		labelSearchStatus = new Label(parent, SWT.None);
		labelSearchStatus.setForeground(new Color(null, 0, 0, 0));
		gridData = new GridData(80, SWT.DEFAULT);
		gridData.horizontalAlignment = SWT.END;
		labelSearchStatus.setLayoutData(gridData);
		labelSearchStatus.setText(""); //$NON-NLS-1$

		labelResult = new Label(parent, SWT.NONE);
		labelResult.setForeground(new Color(null, 0, 0, 0));
		gridData = new GridData(80, SWT.DEFAULT);
		gridData.horizontalAlignment = SWT.END;
		labelResult.setLayoutData(gridData);
		labelResult.setText(resultsCount
				+ Messages.DictionarySearchView_Results);

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);
		Tree tree = viewer.getTree();
		{
			GridData gridData_1 = new GridData(SWT.FILL, SWT.FILL, true, true,
					4, 1);
			gridData_1.heightHint = 364;
			gridData_1.widthHint = 576;
			tree.setLayoutData(gridData_1);
		}
		// GridData gd = new GridData(GridData.FILL, GridData.FILL, true, true);
		// gd.heightHint = 200;
		// gd.minimumHeight = 50;
		// gd.minimumWidth = 100;
		// gd.horizontalSpan = 5;
		Tree control = (Tree) viewer.getControl();
		// control.setLayoutData(gd);
		control.setHeaderVisible(true);
		control.setBackground(new Color(null, 238, 238, 238));

		setDrillDownAdapter(new DrillDownAdapter(viewer));
		TreeColumn column1 = new TreeColumn(viewer.getTree(), SWT.LEFT);
		viewer.getTree().setLinesVisible(false);

		column1.setAlignment(SWT.LEFT);
		column1.setText(Messages.DictionarySearchView_Column_Lemma_Dict);
		column1.setWidth(150);
		if (withWBLE) {
			// not needed in Version 1.0
			TreeColumn column3 = new TreeColumn(viewer.getTree(), SWT.CENTER);
			column3.setAlignment(SWT.CENTER);
			column3.setText(Messages.DictionarySearchView_Column_Link);
			column3.setWidth(35);
		}
		TreeColumn column2 = new TreeColumn(viewer.getTree(), SWT.RIGHT);
		column2.setAlignment(SWT.LEFT);
		column2.setText(Messages.DictionarySearchView_Column_FirstLine);
		column2.setWidth(400);

		// browser = new Browser(parent, SWT.NONE);

		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());
		viewer.getControl().setBackground(new Color(null, 238, 238, 238));
		/*
		 * viewer.getControl().addMouseTrackListener(new MouseTrackAdapter() {
		 * public void mouseHover(MouseEvent e) { Object tempObject =
		 * e.getSource(); if (tempObject instanceof Tree) { Tree tempTree =
		 * (Tree) tempObject; Object tempData = (Object) tempTree.getData(); if
		 * (tempData instanceof DSLemma) { DSLemma tempLemma = (DSLemma)
		 * tempData; if (tempData instanceof DictionaryWoerter) {
		 * showMessage(tempData.getClass().toString());
		 * System.out.println("MouseEvent: " + e.getSource()); } } } } });
		 */

		getSite().setSelectionProvider(viewer);
		mySelectionListener = new MyISelectionListener();
		getSite().getWorkbenchWindow().getSelectionService()
				.addPostSelectionListener(mySelectionListener);
		// getSite().getWorkbenchWindow().getSelectionService()
		// .addSelectionListener(mySelectionListener);

		viewer.expandAll();
		viewer.getTree().addMouseMoveListener(new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {
				// get current TreeItem below the mouse cursor
				TreeItem item = viewer.getTree().getItem(new Point(e.x, e.y));

				// test if we want to highlight it
				if (item != null && item.getData() instanceof DSDictionaryWords) {
					DSDictionaryWords dict = (DSDictionaryWords) item.getData();
					viewer.getTree().setCursor(
							new Cursor(Display.getCurrent(), SWT.CURSOR_HAND));
					viewer.getTree().setToolTipText(dict.getName());
				} else {
					viewer.getTree().setCursor(
							new Cursor(Display.getCurrent(), SWT.CURSOR_ARROW));
					viewer.getTree().setToolTipText(""); //$NON-NLS-1$
				}

			}
		});

		viewer.getTree().addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseDown(MouseEvent e) {
				// This opens the DictionaryLinkEditor when clicked on the 2nd
				// column
				// where the @-sign is shown
				// System.out.println("x:"+e.x+", y:"+e.y);

				// **************************************************************************

				if (withWBLE) {
					// Deactivate call of DLE for TGLAB version 1.0
					// M.Leuk 20.04.2011

					int linkColumnFrom = viewer.getTree().getColumn(0)
							.getWidth();
					int linkColumnTo = linkColumnFrom
							+ viewer.getTree().getColumn(1).getWidth();
					TreeItem item = viewer.getTree().getItem(
							new Point(e.x, e.y));
					if (e.x > linkColumnFrom && e.x < linkColumnTo
							&& item != null
							&& item.getData() instanceof DSDictionaryWords) {
						wantLinkEditorInsteadOfDictBrowser = true;
						DSDictionaryWords dw = (DSDictionaryWords) item
								.getData();

						WBLemma lemma = new WBLemma(dw
								.getAssociatedExactLemma().getName(), dw
								.getName(), dw.getShort(), dw.getBGColor(), dw
								.getColor(), dw.getHref());
						try {
							DictionaryLinkEditor dle = (DictionaryLinkEditor) PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.showView(
											"info.textgrid.lab.dictionarylinkeditor.views.DictionaryLinkEditor"); //$NON-NLS-1$
							dle.setLemma(lemma);
						} catch (PartInitException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					} else {
						wantLinkEditorInsteadOfDictBrowser = false;
					}
				}
			}

			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				if (wantLinkEditorInsteadOfDictBrowser)
					return;
				IHandlerService handlerService = (IHandlerService) getSite()
						.getService(IHandlerService.class);
				try {
					handlerService.executeCommand(
							"info.textgrid.lab.dictionarysearch.showBrowser", //$NON-NLS-1$
							null);
				} catch (CommandException e) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							"Could not execute the showbrowser command for the event " //$NON-NLS-1$
									+ event, e);
					Activator.getDefault().getLog().log(status);
				}
				DictionaryBrowserView dbv = (DictionaryBrowserView) PlatformUI
						.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage().findView(DictionaryBrowserView.ID);
				dbv.selectionChanged(DictionarySearchView.this,
						event.getSelection());

			}

		});

		buttonPrevious = new Button(parent, SWT.PUSH);
		buttonPrevious.setText(Messages.DictionarySearchView_Previous);
		gridData = new GridData(SWT.CENTER);
		gridData.widthHint = 70;
		buttonPrevious.setLayoutData(gridData);
		buttonPrevious.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent e) {
				if (pageNo > 1) {
					int offset = Integer.parseInt(search_Offset);
					int limit = Integer.parseInt(search_Limit);
					// System.out.println("vorher PREV: " + search_Offset + ";"
					// + offset + ";" + pageNo + ";" + pageCount+ "; Limit: "+
					// limit);
					offset = offset - limit;
					search_Offset = "" + offset; //$NON-NLS-1$
					DSLemma nextLemma = new DSLemma(search_Lemma);
					pageNo--;
					Cursor waitCursor = new Cursor(Display.getCurrent(),
							SWT.CURSOR_WAIT);
					parent.setCursor(waitCursor);
					labelSearchStatus.setForeground(new Color(null, 0, 0, 255));
					labelSearchStatus.setText(PROCESSING);
					viewer.setInput(nextLemma);
					viewer.expandAll();
					setNewLabelText(search_Lemma);
					// System.out.println("nachher PREV: " + search_Offset + ";"
					// + offset + ";" + pageNo + ";" + pageCount+ "; Limit: "+
					// limit);
					buttonPrevious.setEnabled(true);
					if (pageNo == 1) {
						buttonPrevious.setEnabled(false);
						buttonNext.setEnabled(true);
					}
					if (pageNo < pageCount) {
						buttonNext.setEnabled(true);
					}
					Cursor normalCursor = new Cursor(Display.getCurrent(),
							SWT.CURSOR_ARROW);
					parent.setCursor(normalCursor);
					labelSearchStatus.setForeground(new Color(null, 0, 0, 0));
					labelSearchStatus.setText(""); //$NON-NLS-1$

				} else {
					buttonPrevious.setEnabled(false);
					viewer.refresh();
				}

			}
		});

		labelPage = new Label(parent, SWT.NONE);
		labelPage.setText(Messages.DictionarySearchView_Page_0_of_0);
		gridData = new GridData(100, SWT.DEFAULT);
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.CENTER;
		gridData.grabExcessHorizontalSpace = true;
		// gridData.widthHint = 100; // changed into 100, formally 75, because
		// of display bug from stefan
		labelPage.setLayoutData(gridData);

		buttonNext = new Button(parent, SWT.PUSH);
		buttonNext.setText(Messages.DictionarySearchView_Next);
		gridData = new GridData(70, SWT.DEFAULT);
		gridData.horizontalAlignment = SWT.END;
		buttonNext.setLayoutData(gridData);

		buttonNext.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				if (pageNo < pageCount) {
					int offset = Integer.parseInt(search_Offset);
					int limit = Integer.parseInt(search_Limit);

					// System.out.println("Suchart: " +search_Suchart);

					offset += limit;
					search_Offset = "" + offset; //$NON-NLS-1$
					DSLemma nextLemma = new DSLemma(search_Lemma);
					pageNo++;
					Cursor waitCursor = new Cursor(Display.getCurrent(),
							SWT.CURSOR_WAIT);
					parent.setCursor(waitCursor);
					labelSearchStatus.setForeground(new Color(null, 0, 0, 255));
					labelSearchStatus.setText(PROCESSING);
					viewer.setInput(nextLemma);
					viewer.expandAll();
					setNewLabelText(search_Lemma);
					buttonNext.setEnabled(true);
					if ((pageCount - pageNo) == 0)
						buttonNext.setEnabled(false);
					if (pageNo == 1)
						buttonPrevious.setEnabled(false);
					// System.out.println("nachher NEXT: " + search_Offset + ";"
					// + offset + ";" + pageNo + ";" + pageCount+ "; Limit: "+
					// limit);
					Cursor normalCursor = new Cursor(Display.getCurrent(),
							SWT.CURSOR_ARROW);
					parent.setCursor(normalCursor);
					labelSearchStatus.setForeground(new Color(null, 0, 0, 0));
					labelSearchStatus.setText(""); //$NON-NLS-1$
				} else {
					buttonNext.setEnabled(false);
					viewer.refresh();
				}

			}
		});

		if (emptyResult) {
			buttonPrevious.setEnabled(false);
			buttonNext.setEnabled(false);
		} else {
			buttonPrevious.setEnabled(false);
			buttonNext.setEnabled(false);
		}

		// makeActions();
		// hookContextMenu();
		// hookDoubleClickAction();
		// contributeToActionBars();

		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(parent,
						"info.textgrid.lab.dictionarysearch.DictionarySearchResultView"); //$NON-NLS-1$
	}

	// private void hookContextMenu() {
	// MenuManager menuMgr = new MenuManager("#PopupMenu");
	// menuMgr.setRemoveAllWhenShown(true);
	// menuMgr.addMenuListener(new IMenuListener() {
	// public void menuAboutToShow(IMenuManager manager) {
	// DictionarySearchView.this.fillContextMenu(manager);
	// }
	// });
	// Menu menu = menuMgr.createContextMenu(viewer.getControl());
	// viewer.getControl().setMenu(menu);
	// getSite().registerContextMenu(menuMgr, viewer);
	// }

	// private void contributeToActionBars() {
	// IActionBars bars = getViewSite().getActionBars();
	// fillLocalPullDown(bars.getMenuManager());
	// fillLocalToolBar(bars.getToolBarManager());
	// }

	// private void fillLocalPullDown(IMenuManager manager) {
	// manager.add(action1);
	// manager.add(new Separator());
	// manager.add(action2);
	// }

	// private void fillContextMenu(IMenuManager manager) {
	// manager.add(action1);
	// manager.add(action2);
	// manager.add(new Separator());
	// drillDownAdapter.addNavigationActions(manager);
	// // Other plug-ins can contribute there actions here
	// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	// }

	// private void fillLocalToolBar(IToolBarManager manager) {
	// manager.add(action1);
	// manager.add(action2);
	// manager.add(new Separator());
	// drillDownAdapter.addNavigationActions(manager);
	// }

	// private void makeActions() {
	// action1 = new Action() {
	// @Override
	// public void run() {
	// // //showMessage("Action 1 executed");
	// }
	// };
	// action1.setText("Action 1");
	// action1.setToolTipText("Action 1 tooltip");
	// action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
	// .getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
	//
	// action2 = new Action() {
	// @Override
	// public void run() {
	// // //showMessage("Action 2 executed");
	// }
	// };
	// action2.setText("Action 2");
	// action2.setToolTipText("Action 2 tooltip");
	// action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
	// .getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
	// doubleClickAction = new Action() {
	// @Override
	// public void run() {
	// // showMessage("Entering matches for " + thisSearchLemma);
	// // uiJob = new EnterDictionariesUIJob("Entering matches for " +
	// // thisSearchLemma);
	// // uiJob.schedule();
	// // ISelection selection = viewer.getSelection();
	// // Object obj = ((IStructuredSelection) selection)
	// // .getFirstElement();
	// // viewer.setSelection(selection, true);
	// // // ////showMessage("Double-click detected on " +
	// // // obj.toString());
	// // if (obj instanceof DictionaryWoerter) {
	// // /*
	// // * System.out.println("URL: " + ((DictionaryWoerter)
	// // * obj).getHref());
	// // */
	// // DictionaryBrowserView dbv = (DictionaryBrowserView) getSite()
	// // .getPage().findView(DictionaryBrowserView.ID);
	// // if (dbv == null)
	// // dbv = (DictionaryBrowserView) PlatformUI.getWorkbench()
	// // .getActiveWorkbenchWindow().getActivePage()
	// // .findView(DictionaryBrowserView.ID);
	// // if (dbv == null)
	// // try {
	// // dbv = (DictionaryBrowserView) getSite().getPage()
	// // .showView(DictionaryBrowserView.ID);
	// // // does not return null, so it's either success or
	// // // an exception.
	// // } catch (PartInitException e) {
	// // }
	// // dbv.selectionChanged(DictionarySearchView.this, selection);
	// // }
	//
	// }
	// };
	// }
	//
	// private void hookDoubleClickAction() {
	// viewer.addDoubleClickListener(new IDoubleClickListener() {
	// public void doubleClick(DoubleClickEvent event) {
	// doubleClickAction.run();
	// }
	// });
	// }

	private void showMessage(String message) {
		MessageDialog.openInformation(this.getSite().getShell(),
				Messages.DictionarySearchView_View_Name, message);
		// viewer.getControl().getShell()
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		if (selection instanceof IStructuredSelection) {
			Object o = ((IStructuredSelection) selection).getFirstElement();
			if (o instanceof String) {
				String tempLemma = (String) o;
				// System.out.println("It contains a dictionaryWoerter: "
				// + dictionary.toString());
				DictionarySearchView.this.setContentDescription(tempLemma);
				DSLemma newLemma = new DSLemma(tempLemma);
				viewer.setInput(newLemma);
			}
		}

	}

	/**
	 * 
	 * @param lemma
	 *            searchlemma
	 */
	public void getResults4SearchLemma(String lemma) {
		DSLemma newLemma = new DSLemma(lemma);
		this.setOffset("0"); //$NON-NLS-1$
		this.setPageNo(1);
		this.setSearchFromOutside(false);
		this.getTreeViewer().setInput(newLemma);
		this.getTreeViewer().expandAll();
		this.setNewLabelText(lemma);
	}

	/**
	 * sets the label "lemma" on top of the view according to the searchword
	 * 
	 * @param text
	 */
	public void setNewLabelText(String text) {
		labelLemma.setText(text);
		labelResult.setText(resultsCount
				+ Messages.DictionarySearchView_Results);
		/*
		 * Bugfix TG-536
		 * https://develop.sub.uni-goettingen.de/jira/browse/TG-536 When
		 * searching for a nonexistent expression like e.g. "lsmwf", no result
		 * is found, but there is still displayed "page 1 of 2". This only
		 * occurs after having received results with 2 display pages. Problem
		 * resolved
		 */
		if (resultsCount > 0) {
			if (resultsCount % Integer.parseInt(search_Limit) == 0)
				pageCount = resultsCount / Integer.parseInt(search_Limit);
			else
				pageCount = resultsCount / Integer.parseInt(search_Limit) + 1;
			labelPage.setText(Messages.DictionarySearchView_Page + pageNo
					+ Messages.DictionarySearchView_Of + pageCount);
		}
		if (resultsCount == 0)
			labelPage.setText(Messages.DictionarySearchView_Page
					+ "0" + Messages.DictionarySearchView_Of + "0"); //$NON-NLS-2$ //$NON-NLS-4$
	}

	/**
	 * sets the searchtype for the search in the "Trierer W�rterbuchnetz"
	 * 
	 * @param searchType
	 */
	public void setSearchType(String searchType) {
		search_Suchart = searchType;
	}

	public void setSearchFromOutside(Boolean searchFromOutside) {
		SEARCH_FROM_OUTSIDE = searchFromOutside;
	}

	/**
	 * sets the Offset for the search in the "Trierer W�rterbuchnetz"
	 * 
	 * @param offset
	 */
	public void setOffset(String offset) {
		search_Offset = offset;
	}

	/**
	 * sets the Limit for the search in the "Trierer W�rterbuchnetz"
	 * 
	 * @param limit
	 */
	public void setLimit(String limit) {
		search_Limit = "" + limit; //$NON-NLS-1$
	}

	/**
	 * sets the WbbList (List of the dictionaries to search in) for the search
	 * in the "Trierer W�rterbuchnetz"
	 * 
	 * @param wbbList
	 */
	public void setWbbList(String wbbList) {
		search_Wbblist = wbbList;
	}

	/**
	 * sets the boolean "setLinkedLemmas" parameter for the search in the
	 * "Trierer W�rterbuchnetz"
	 * 
	 * @param linkedlemmas
	 */
	public void setLinkedLemmas(String linkedlemmas) {
		SEARCH_LINKEDLEMMAS = linkedlemmas;
	}

	/**
	 * sets the page number parameter for the search in the
	 * "Trierer W�rterbuchnetz"
	 * 
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setDrillDownAdapter(DrillDownAdapter drillDownAdapter) {
		this.drillDownAdapter = drillDownAdapter;
	}

	public DrillDownAdapter getDrillDownAdapter() {
		return drillDownAdapter;
	}

	public void setThisSearchLemma(String thisSearchLemma) {
		this.thisSearchLemma = thisSearchLemma;
	}

	public String getThisSearchLemma() {
		return thisSearchLemma;
	}

	private void closePerspective() {
		IWorkbench wb = PlatformUI.getWorkbench();
		wb.getActiveWorkbenchWindow()
				.getActivePage()
				.closePerspective(
						wb.getPerspectiveRegistry()
								.findPerspectiveWithId(
										"info.textgrid.lab.woerterbuchsearchmask.perspectives.RelEngPerspective"), false, true); //$NON-NLS-1$
	}

	public boolean checkWebService() {
		String URL = TG_WOERTERBUCH_WEBSERVICE_UNI_TRIER;
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URL)
					.openConnection();
			con.setRequestMethod("GET"); //$NON-NLS-1$
			con.setReadTimeout(READ_TIME_OUT);
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				return true;
			} else {
				return false;
			}
		} catch (IOException ex) {
			System.out.println("FEHLER!!!!!!!!!!!"); //$NON-NLS-1$
			return false;
		}
	}

}