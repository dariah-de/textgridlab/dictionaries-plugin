package info.textgrid.lab.dictionarysearch.views;



import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;

public class newSearchHandler extends AbstractHandler implements IHandler {

	private ISelection selection;

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		IEditorPart part = HandlerUtil.getActiveEditor(event);
		if (part != null) {
			ITextEditor editor =
				(ITextEditor) part.getAdapter(ITextEditor.class);
			if (editor != null) {
				selection =	editor.getSelectionProvider().getSelection();
				//String text = ((ITextSelection) selection).getText();

			}
			
			try {
				IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				if (activePage == null) 
					MessageDialog
					.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), Messages.newSearchHandler_No_Open_Perspectice,
							Messages.newSearchHandler__No_Open_Perspective_Message);
				else 
					//activePage.showView("info.textgrid.lab.dictionarysearch.views.DictionarySearchView");
					//
					((info.textgrid.lab.dictionarysearch.views.DictionarySearchView) PlatformUI
						.getWorkbench()
						.getActiveWorkbenchWindow()
						.getActivePage()
						.showView(
								"info.textgrid.lab.dictionarysearch.views.DictionarySearchView", //$NON-NLS-1$
								null, IWorkbenchPage.VIEW_ACTIVATE))
						.getResults4SearchLemma(((ITextSelection) selection).getText());
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(Messages.newSearchHandler_Could_Not_Open_DRV);
				e.printStackTrace();
			}
		}
		return null;
	}

}
