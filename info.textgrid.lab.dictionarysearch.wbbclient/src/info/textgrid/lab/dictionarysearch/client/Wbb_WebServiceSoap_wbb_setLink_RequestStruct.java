// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, Build R2)
// Generated source version: 1.1.3

package info.textgrid.lab.dictionarysearch.client;

public class Wbb_WebServiceSoap_wbb_setLink_RequestStruct {
	protected java.lang.String sourceID;
	protected java.lang.String sourcefolder;
	protected java.lang.String targetID;
	protected java.lang.String targetfolder;
	protected java.lang.String bName;
	protected java.lang.String quality;
	protected java.lang.String comment;
	protected java.lang.String semanticRelation;
	protected java.lang.String etymRelation;
	protected java.lang.String SID;
	protected java.lang.String approved;
	protected java.lang.String deleted;
	protected java.lang.String editable;

	public Wbb_WebServiceSoap_wbb_setLink_RequestStruct() {
	}

	public Wbb_WebServiceSoap_wbb_setLink_RequestStruct(
			java.lang.String sourceID, java.lang.String sourcefolder,
			java.lang.String targetID, java.lang.String targetfolder,
			java.lang.String bName, java.lang.String quality,
			java.lang.String comment, java.lang.String semanticRelation,
			java.lang.String etymRelation, java.lang.String SID,
			java.lang.String approved, java.lang.String deleted,
			java.lang.String editable) {
		this.sourceID = sourceID;
		this.sourcefolder = sourcefolder;
		this.targetID = targetID;
		this.targetfolder = targetfolder;
		this.bName = bName;
		this.quality = quality;
		this.comment = comment;
		this.semanticRelation = semanticRelation;
		this.etymRelation = etymRelation;
		this.SID = SID;
		this.approved = approved;
		this.deleted = deleted;
		this.editable = editable;
	}

	public java.lang.String getSourceID() {
		return sourceID;
	}

	public void setSourceID(java.lang.String sourceID) {
		this.sourceID = sourceID;
	}

	public java.lang.String getSourcefolder() {
		return sourcefolder;
	}

	public void setSourcefolder(java.lang.String sourcefolder) {
		this.sourcefolder = sourcefolder;
	}

	public java.lang.String getTargetID() {
		return targetID;
	}

	public void setTargetID(java.lang.String targetID) {
		this.targetID = targetID;
	}

	public java.lang.String getTargetfolder() {
		return targetfolder;
	}

	public void setTargetfolder(java.lang.String targetfolder) {
		this.targetfolder = targetfolder;
	}

	public java.lang.String getBName() {
		return bName;
	}

	public void setBName(java.lang.String bName) {
		this.bName = bName;
	}

	public java.lang.String getQuality() {
		return quality;
	}

	public void setQuality(java.lang.String quality) {
		this.quality = quality;
	}

	public java.lang.String getComment() {
		return comment;
	}

	public void setComment(java.lang.String comment) {
		this.comment = comment;
	}

	public java.lang.String getSemanticRelation() {
		return semanticRelation;
	}

	public void setSemanticRelation(java.lang.String semanticRelation) {
		this.semanticRelation = semanticRelation;
	}

	public java.lang.String getEtymRelation() {
		return etymRelation;
	}

	public void setEtymRelation(java.lang.String etymRelation) {
		this.etymRelation = etymRelation;
	}

	public java.lang.String getSID() {
		return SID;
	}

	public void setSID(java.lang.String SID) {
		this.SID = SID;
	}

	public java.lang.String getApproved() {
		return approved;
	}

	public void setApproved(java.lang.String approved) {
		this.approved = approved;
	}

	public java.lang.String getDeleted() {
		return deleted;
	}

	public void setDeleted(java.lang.String deleted) {
		this.deleted = deleted;
	}

	public java.lang.String getEditable() {
		return editable;
	}

	public void setEditable(java.lang.String editable) {
		this.editable = editable;
	}
}
