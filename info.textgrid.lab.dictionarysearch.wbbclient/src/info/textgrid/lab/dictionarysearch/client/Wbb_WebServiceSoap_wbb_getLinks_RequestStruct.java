// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, Build R2)
// Generated source version: 1.1.3

package info.textgrid.lab.dictionarysearch.client;

public class Wbb_WebServiceSoap_wbb_getLinks_RequestStruct {
	protected java.lang.String sourceid;

	public Wbb_WebServiceSoap_wbb_getLinks_RequestStruct() {
	}

	public Wbb_WebServiceSoap_wbb_getLinks_RequestStruct(
			java.lang.String sourceid) {
		this.sourceid = sourceid;
	}

	public java.lang.String getSourceid() {
		return sourceid;
	}

	public void setSourceid(java.lang.String sourceid) {
		this.sourceid = sourceid;
	}
}
